from pathlib import Path
import sys
import shutil
import subprocess

# this TMP directory will be deleted and recreated! Therefore all files will be lost!
DEFAULT_TMP_DIR = "/tmp/viper/out"
DEFAULT_TEST_DIR = "experiments/test_suites"

PRINT_PROCESS_OUTPUT = True

FAIL = '\033[91m'
ENDC = '\033[0m'
OKGREEN = '\033[92m'


def run_test_case(case: Path, output_dir: Path) -> bool:
    if case is None or not case.is_dir() or not (case / "viper").is_dir():
        # is not a test case, therefore it can not fail
        return True

    if output_dir.is_dir():
        shutil.rmtree(output_dir)

    output_dir.mkdir(parents=True)

    print("Start Testset: {path}".format(path=case))
    proc = subprocess.run(["./experiments/run_test.sh", "+output", str(case.absolute())], stdout=subprocess.PIPE)
    res = proc.returncode
    if PRINT_PROCESS_OUTPUT:
        print(proc.stdout.decode("utf-8"), end="")
    return res == 0


if __name__ == '__main__':

    if len(sys.argv) > 2:
        test_dir = Path(sys.argv[1])
        tmp_dir = Path(sys.argv[2])
    else:
        test_dir = Path(DEFAULT_TEST_DIR)
        tmp_dir = Path(DEFAULT_TMP_DIR)

    nr_of_tests = 0
    nr_of_successes = 0

    for dir in test_dir.iterdir():
        if not dir.is_dir():
            continue

        nr_of_tests += 1

        if run_test_case(dir, tmp_dir):
            result = OKGREEN + "success" + ENDC
            nr_of_successes += 1
        else:
            result = FAIL + "failed" + ENDC

        print("Test: {nr} - {dir} [{result}]".format(nr=str(nr_of_tests), dir=str(dir), result=result))

    print("{suc}/{tests} succeeded!".format(suc=nr_of_successes, tests=nr_of_tests))
    if nr_of_tests == nr_of_successes:
        exit(0)
    else:
        exit(1)
