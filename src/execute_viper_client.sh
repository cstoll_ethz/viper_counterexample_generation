#!/bin/bash
cd "$(dirname "$0")"
#####################################################
##### USAGE

usage="usage: execute_viper_client.sh FILE_TO_VERIFY"

if [ -z "$1" ]; then
	echo ${usage}
	exit -1
fi

#####################################################
##### SETTINGS

tmp_dir="/tmp/viper/"
result_dir="${tmp_dir}result/"
tmp_file="output.log"

z3Path="$(realpath "/usr/local/Viper/z3/bin/z3")"
#z3Path="z3"

#####################################################

#clear old result directory
if [[ -d "${result_dir}" ]]; then
	rm -r "${result_dir}" 
fi
mkdir -p "${result_dir}"

#run the verifier
options="--writeTraceFile --ideModeAdvanced"
#options="--writeTraceFile"
python3 ../src/viper_client/client.py -p 7770 -v silicon -f "$1" -x "--z3LogFile \"${result_dir}${tmp_file}\" ${options} --numberOfParallelVerifiers 1 --disableCaching --z3Exe \"${z3Path}\"" > "${result_dir}viper_client.log"

if (($?)); then
	echo "Error while executing viper_client.py"
	exit -1
fi

#for some reason Silicon logs these two files to the startlocation of the server
script_dir="../tools/"

dot_file="${script_dir}dot_input.dot"
if [ -f "${dot_file}" ]; then
	mv "${dot_file}" "${result_dir}"
fi

symbExLog="${script_dir}executionTreeData.js"
if [ -f "${symbExLog}" ]; then
	mv "${symbExLog}" "${result_dir}"
fi

#output the generated smt2 code
cat "${result_dir}${tmp_file}-01.smt2"

