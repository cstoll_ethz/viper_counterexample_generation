#!/bin/bash
################################################################
##### USAGE

usage="usage: generate_counterexample.sh VIPERFILE [RESULT_DIR]"
if [ -z "$1" ]; then
	echo "${usage}"
	exit -1
fi

################################################################
##### SETTINGS

python37="python3.7"
tmp_directory="/tmp/viper/result/"

################################################################

search_replace="./search_replace.py"
viper_file="$(realpath "$1")"

result_dir="out/"
if [ -n "$2" ]; then
	result_dir="$(realpath "$2")/"
fi

if [ ! -f "$1" ]; then
	echo -e "${RED}File not found '$1'${NC}"
	exit -1
fi

cd "$(dirname "$0")"

if [ -d "${result_dir}" ]; then
		rm -r "${result_dir}"
fi
mkdir "${result_dir}"

name=$(basename "${viper_file}" ".vpr")

#copy viper file
code_file="${result_dir}code.vpr"
cp "${viper_file}" "${code_file}"

#Run Silicon with the viper file
result=$("./execute_viper_client.sh" "$(realpath ${code_file})")
if (($?)); then
	echo -e "${RED}Error while executing 'execute_viper_client.sh'\n${result}${NC}"
	exit -1
fi
#save raw smt2 output
raw_smt2="${result_dir}raw.smt2"
echo "${result}" > "${raw_smt2}"

#save the SymbExLog
symbExLog_old="${tmp_directory}executionTreeData.js"
symbExLog="${result_dir}symbExLog.json"
if [ ! -f "${symbExLog_old}" ]; then
	echo -e "${RED}Could not find the symbExLog '${symbExLog_old}'.${NC}"
#	exit -1
else
	# TODO js json is not in correct python json format. This fix does only work if the input viper file does not contain the string '"oldHeap":,'
	${python37} "${search_replace}" "\"oldHeap\":," "\"oldHeap\":[]," < "${symbExLog_old}" | ${python37} "${search_replace}" "var executionTreeData = " "" > "${symbExLog_old}.v2"
	cp "${symbExLog_old}.v2" "${symbExLog}" 
	if (($?)); then
		echo -e "${RED}Could not copy the fixed symbExLog.${NC}"
		exit -1
	fi
fi

viper_client_log_file="${result_dir}viper_client.log"
cp "${tmp_directory}viper_client.log" "${viper_client_log_file}"

${python37} "../src/counterexample_generation/main.py" "${result_dir}"
if (($?)); then
	echo -e "${RED}Error while processing the viper file.${NC}"
	exit -1
fi
