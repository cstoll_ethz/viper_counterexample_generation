import json
import logging
import sys
import time
from pathlib import Path
from typing import List, Optional, Dict

import counterexample
import function_application_graph
import visualisation
from inputparsing import input_parser
import z3_solver
import inputparsing.smt_parser as smt_parser
import inputparsing.z3_model_parser as z3_model_parser
import inputparsing.symbex_log_parser as symbex_log_parser
from inputparsing.infix_to_smt_converter import infix_to_smt


def _get_user_defined_name(internal_name: str) -> str:
    if "@" in internal_name:
        return internal_name.split("@")[0]
    return internal_name


def _get_internal_numbering(internal_name: str) -> int:
    if "@" in internal_name:
        return int(internal_name.split("@")[1])
    return 1


def _remove_silicon_internals(d: Dict) -> Dict:
    # TODO: Remove this Assumptions!
    #  We assume that no user defined variable/function starts with a "$"
    #  find other ways for the inv and Set functions
    return {key: value for key, value in d.items()
            if not key.startswith("$")
            and not key.startswith("inv@")
            and not key.startswith("Set_")
            # and not key.startswith("sm@")
            and not key.endswith("%limited")
            and not key.endswith("%stateless")
            and not key.endswith("%trigger")
            and not key.startswith("s@$")
            }


def _build_store(ce: counterexample.CE):
    start_time = time.time()

    relevant_constants = ce.sources.symbex_store

    constants = [t for _, t in relevant_constants.items()]

    store: List[counterexample.CE.Variable] = []
    for c, c_type in constants:
        if c in ce.sources.z3_model.entries:
            val = ce.sources.z3_model.entries[c].constant_value()
        else:
            val = c

        # quick fix (maybe there are more problems like this)
        if c_type == "Ref":
            c_type = "$Ref"

        v = counterexample.CE.Variable(c_type, c, val)
        store.append(v)

    ce.store = store

    end_time = time.time()
    logging.debug("build_store running time: " + str(round(end_time - start_time, 5)) + "s")


def _partial_apply_lookup_function(qp: symbex_log_parser.SymbexLogHeap.QuantifiedPermission, ce: counterexample.CE) \
        -> Optional[counterexample.CE.Function]:
    fvf_lookup = "$FVF.lookup_" + qp.field
    z3_model = ce.sources.z3_model

    if fvf_lookup not in z3_model.entries:
        return None
    lookup_function = z3_model.entries[fvf_lookup]
    lookup_decl = ce.sources.smt_file.function_declarations[fvf_lookup]

    smt_value = z3_model.get_value(qp.value)

    new_cases: List[counterexample.CE.FunctionCase] = []
    for case in lookup_function.cases:
        argument_list = case[0]
        if len(argument_list) < 1:
            continue

        if str(argument_list[0]) == "else":
            new_cases.append(case)

        if str(argument_list[0]) == smt_value:
            new_cases.append((argument_list[1:], case[1]))
        else:
            # do nothing, skip this case
            pass

    return counterexample.CE.Function(fvf_lookup + "_" + smt_value,
                                      lookup_decl.parameter_types,
                                      lookup_decl.return_type, new_cases)


def _check_permission_for_qp_cases(qp: symbex_log_parser.SymbexLogHeap.QuantifiedPermission,
                                   partial_func: counterexample.CE.Function,
                                   inverse_expression_dict: Dict[str, smt_parser.Exp],
                                   solver: z3_solver.Z3Solver) -> counterexample.CE.Function:
    last_question_mark = qp.perm_amount.rfind("?")
    perm_infix = qp.perm_amount[1:last_question_mark].strip()
    logging.debug(perm_infix)

    exp = infix_to_smt(perm_infix)

    if not isinstance(exp, smt_parser.FunctionApplication) or exp.name != "implies":
        # the following heuristic does not apply in this case as it only works on "implies" expressions
        return partial_func

    exp = exp.args[0]

    filtered_cases = []

    # case: Tuple[List[Value], Value]
    for case in partial_func.cases:
        lookup_value = case[0][0].value
        if lookup_value in inverse_expression_dict:
            lookup_term = inverse_expression_dict[lookup_value]
        else:
            lookup_term = smt_parser.Constant(lookup_value)
        perm_exp = exp.assign_temp_variables_in_exp({qp.variable: lookup_term})
        result = solver.eval(perm_exp.smt_string)
        if result is None:
            # TODO add special marker for this cases
            filtered_cases.append(case)
        elif result == "true":
            filtered_cases.append(case)

    return counterexample.CE.Function(partial_func.name, partial_func.parameter_list, partial_func.return_type,
                                      filtered_cases)


def _build_heap(ce: counterexample.CE, solver: z3_solver.Z3Solver):
    start_time = time.time()

    symbexlog_heap = ce.sources.symbex_heap
    predicate_instances: List[counterexample.CE.PredicateInstance] = []

    for pred in symbexlog_heap.predicate_instances:
        predicate_instances.append(counterexample.CE.PredicateInstance(pred.predicate, pred.snapshot, pred.arguments))

    others = []
    for other in symbexlog_heap.others:
        others.append(counterexample.CE.OtherHeapEntry(other.entry))

    permissions = []
    for perm in symbexlog_heap.permissions:
        # TODO read field_type from Viper file
        value_type = None
        if perm.heap_chunk in ce.sources.z3_model.entries:
            heap_chunk_value = ce.sources.z3_model.entries[perm.heap_chunk].constant_value()
        else:
            heap_chunk_value = perm.heap_chunk
        if perm.value in ce.sources.z3_model.entries:
            model_value = ce.sources.z3_model.entries[perm.value].constant_value()
        else:
            model_value = perm.value
        permissions.append(
            counterexample.CE.Permission(perm.heap_chunk, heap_chunk_value, perm.field, perm.value, model_value,
                                         value_type,
                                         perm.perm_amount))

    quantified_permissions = []
    for qp in symbexlog_heap.quantified_permissions:
        new_func = _partial_apply_lookup_function(qp, ce)
        if new_func is None:
            continue

        # check the for permission
        new_func = _check_permission_for_qp_cases(qp, new_func, ce.inverse_dict, solver)

        # TODO read field_type from Viper file
        value_type = None
        model_value = ce.sources.z3_model.entries[qp.value]
        qp = counterexample.CE.QuantifiedPermission(qp.field, qp.value, model_value, value_type, new_func)
        quantified_permissions.append(qp)

    ce.heap = counterexample.CE.Heap(permissions, quantified_permissions, predicate_instances, others)

    end_time = time.time()
    logging.debug("build_heap running time: " + str(round(end_time - start_time, 5)) + "s")


def _build_functions(ce: counterexample.CE):
    start_time = time.time()

    filtered_functions: Dict[str, smt_parser.FunctionDeclaration] = _remove_silicon_internals(
        ce.sources.smt_file.function_declarations)

    ce.functions: List[counterexample.CE.Function] = []
    for name, fun_decl in filtered_functions.items():
        if name not in ce.sources.z3_model.entries:
            continue

        func = ce.sources.z3_model.entries[name]

        function = counterexample.CE.Function(name, fun_decl.parameter_types, fun_decl.return_type, func.cases)
        ce.functions.append(function)
    end_time = time.time()
    logging.debug("build_functions running time: " + str(round(end_time - start_time, 5)) + "s")


def _init_z3_solver(smt_query: str, ce: counterexample.CE) -> z3_solver.Z3Solver:
    msg = []
    lines = smt_query.split("\n")
    # execute all statements until we reach the first "(get-model)" (the smt query should only contain one of it)
    for line in lines:
        if line.startswith("(get-model)"):
            break

        msg.append(line)

    solver: z3_solver.Z3Solver = z3_solver.Z3Solver(debug_name="Main Solver", store_inverse_expression=True,
                                                    inverse_expression_dict=ce.inverse_dict)
    solver.init_session("\n".join(msg))

    return solver


def _generate_counterexample(smt_file: smt_parser.SMTFile, z3_model: z3_model_parser.Model,
                             symbex_heap: symbex_log_parser.SymbexLogHeap,
                             symbex_store: Dict[str, str],
                             failing_method_name: str,
                             simplified: bool = False) -> counterexample.CE:
    inverse_expression_dict: Dict[str, smt_parser.Exp] = {}
    ce = counterexample.CE(smt_file, z3_model, symbex_heap, symbex_store, failing_method_name, inverse_expression_dict)

    solver = _init_z3_solver(ce.sources.smt_file.smt_query, ce)
    try:
        _build_store(ce)

        # init inverse_dict with store variables
        for v in ce.store:
            ce.inverse_dict[v.model_value] = v.name

        _build_functions(ce)

        function_application_graph.build_function_application_graph(ce, solver, simplified=simplified)

        # with the current implementation of the QP filtering the heap construction does only work
        # after we have a completed the inverse_expression_dict (therefore we have to do it after the
        # function graph has been built)
        _build_heap(ce, solver)

    finally:
        solver.stop()

    return ce


def generate_counterexample(method_dir: Path) -> counterexample.CE:
    symbexlog_file = method_dir.parent / "symbExLog.json"
    z3_output_file = method_dir / "z3.out"
    smt_file = method_dir / "method.smt2"
    error_file = method_dir / "error.json"

    smt_file, model, symbex_heap, symbex_store, failing_method = input_parser.parse_input(z3_output_file,
                                                                                          symbexlog_file,
                                                                                          error_file, smt_file)

    return _generate_counterexample(smt_file, model, symbex_heap, symbex_store, failing_method)


class MyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, set):
            return list(obj)
        return json.JSONEncoder.default(self, obj)


def counterexample_to_text(ce: counterexample.CE):
    data = {}
    data["store"] = ce.store
    data["heap"] = ce.heap
    data["functions"] = ce.functions
    return json.dumps(data, cls=MyEncoder, default=lambda x: x.__dict__)


def debug_program_entry():
    if len(sys.argv) == 2:
        method_dir = Path(sys.argv[1])
    else:
        logging.error("Usage: counterexample_generator.py 'test_method_dir'")
        exit(-1)

    ce = generate_counterexample(method_dir)
    ce.simplify()
    # print(counterexample_to_text(ce))

    print(visualisation.Visualisation().create_dot_file(ce))


if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout, format='%(name)s - %(levelname)s - %(message)s')
    logging.getLogger("Z3Solver_Matching Solver").setLevel(logging.INFO)
    logging.getLogger("Z3Solver_Main Solver").setLevel(logging.INFO)
    debug_program_entry()
