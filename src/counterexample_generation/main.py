import logging
import subprocess
import sys
import time
from pathlib import Path

import counterexample_generator
import input_preparation
from counterexample import CE
from visualisation import Visualisation


def run_graphviz(ce_dot_file: Path):
    start_time = time.time()

    subprocess.run(["dot", "-Tpng", str(ce_dot_file.absolute()), "-O"]).check_returncode()

    end_time = time.time()
    logging.debug("run graphviz in " + str(round(end_time - start_time, 5)) + "s.")


def main(test_case: Path):
    logging.debug("Entered Python program")
    p_start = time.time()

    failing_methods = input_preparation.prepare(test_case)

    if len(failing_methods) == 0:
        logging.info("No verification errors detected!")

    for name in failing_methods:
        method_dir = test_case / name

        start_time = time.time()
        ce: CE = counterexample_generator.generate_counterexample(method_dir)
        end_time = time.time()
        logging.debug("Created the counterexample in " + str(round(end_time - start_time, 5)) + "s.")

        ce_text = method_dir / "ce.json"
        ce_text.write_text(counterexample_generator.counterexample_to_text(ce))

        ce_dot = method_dir / "ce.dot"
        ce_dot.write_text(Visualisation().create_dot_file(ce))
        run_graphviz(ce_dot)

        ce.simplify()
        ce_simplified = method_dir / "simple.dot"
        ce_simplified.write_text(Visualisation().create_dot_file(ce))
        run_graphviz(ce_simplified)

        ce_without_graph = method_dir / "simple_without_graph.dot"
        ce_without_graph.write_text(Visualisation().create_dot_file(ce, include_function_graph=False))
        run_graphviz(ce_without_graph)

    p_end = time.time()
    logging.debug("Left Python program after " + str(round(p_end - p_start, 5)) + "s.")
    pass


def enable_logging():
    logging.basicConfig(stream=sys.stdout, format='%(name)s - %(levelname)s - %(message)s', level=logging.INFO)
    logging.getLogger("Z3Solver_Matching Solver").setLevel(logging.INFO)
    logging.getLogger("Z3Solver_Main Solver").setLevel(logging.INFO)


if __name__ == '__main__':
    enable_logging()

    main(Path(sys.argv[1]))
