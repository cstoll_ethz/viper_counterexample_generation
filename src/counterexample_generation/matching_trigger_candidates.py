from __future__ import annotations
from typing import TypeVar, Generic, List, Dict, Set

from z3_solver import Z3Solver
from inputparsing.z3_model_parser import Model

T = TypeVar('T')


class CandidateMatching(Generic[T]):
    def __init__(self):
        self.values: Dict[T, int] = {}
        self.id_to_value: Dict[int, T] = {}
        self.variables: Set[str] = set()

        self.solver = Z3Solver(debug_name="Matching Solver")

    def _build_base_smt_query(self, candidates: List[List[Dict[str, T]]]):
        base_smt = ""
        for var in self.variables:
            base_smt += "(declare-const {var} Int)\n".format(var=var)

        combination = []
        for group in candidates:
            group_smt = []

            for assignment in group:
                assign_smt = []
                for var, value in assignment.items():
                    value_id = self.values[value]
                    assign_smt.append("(= {var} {val})".format(var=var, val=value_id))

                group_smt.append("(and {x})".format(x=" ".join(assign_smt)))

            combination.append("(or {x})".format(x=" ".join(group_smt)))

        base_smt += "(assert (and {x}))".format(x=" ".join(combination))
        return base_smt

    def _add_negated_model(self, model: Model):
        neg_model = ""

        assignments = []
        for var, value_id in model.entries.items():
            assignments.append("(= {var} {value_id})".format(var=var, value_id=value_id))

        neg_model += "(not (and {x}))".format(x=" ".join(assignments))
        smt = "(assert {exp})".format(exp=neg_model)

        self.solver.send(smt)

    def _prepare_matching(self, candidates: List[List[Dict[str, T]]]):
        self.values: Dict[T, int] = {}
        self.id_to_value: Dict[int, T] = {}
        self.variables: Set[str] = set()

        counter = 0
        for group in candidates:
            for assignment in group:
                for var, value in assignment.items():

                    self.variables.add(var)

                    if value in self.values:
                        continue

                    self.values[value] = counter
                    self.id_to_value[counter] = value
                    counter += 1

    def get_all_matching_assignments(self, candidates: List[List[Dict[str, T]]]) -> List[Dict[str, T]]:
        self._prepare_matching(candidates)
        base_smt = self._build_base_smt_query(candidates)

        self.solver.send("(push)")
        self.solver.send(base_smt)

        result: List[Dict[str, T]] = []
        sat = self.solver.check_sat()

        while sat:
            model: Model = self.solver.get_model()
            solution = {}
            for key, value_id in model.entries.items():
                value_id = int(value_id.constant_value())
                solution[key] = self.id_to_value[value_id]

            if len(solution) == 0:
                break

            result.append(solution)

            self._add_negated_model(model)
            sat = self.solver.check_sat()

        self.solver.send("(pop)")
        return result

