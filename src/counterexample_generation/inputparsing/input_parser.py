import json
from pathlib import Path

import inputparsing.smt_parser as smt_parser
import inputparsing.z3_model_parser as z3_model_parser
import inputparsing.symbex_log_parser as symbex_log_parser


def get_failing_function_or_method_name(error_file: Path) -> str:
    data = json.loads(error_file.read_text())

    return data["entity"]["name"]


def parse_input(z3_output_file: Path, symbexlog_file: Path, error_file: Path, smt_file: Path):
    if not z3_output_file.is_file():
        raise ValueError(str(z3_output_file) + " is not a file")
    if not symbexlog_file.is_file():
        raise ValueError(str(symbexlog_file) + " is not a file")
    if not error_file.is_file():
        raise ValueError(str(error_file) + " is not a file")
    if not smt_file.is_file():
        raise ValueError(str(smt_file) + " is not a file")

    smt_file = smt_parser.parse_smt_file(smt_file)

    model = z3_model_parser.get_model_from_z3_output(z3_output_file)

    failing_method = get_failing_function_or_method_name(error_file)

    symbexlog_heap, symbexlog_store = symbex_log_parser.parse(symbexlog_file, failing_method)

    return smt_file, model, symbexlog_heap, symbexlog_store, failing_method
