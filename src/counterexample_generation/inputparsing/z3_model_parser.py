import logging
from pathlib import Path
from typing import List, Tuple, Optional, Dict
from itertools import starmap

from inputparsing import tokenizer
from inputparsing.parser import Parser


class Value:
    pass


class Constant(Value):
    def __init__(self, value: str):
        self.value = value

    def __repr__(self):
        return self.value


class FuncApplication(Value):
    def __init__(self, function_name: Constant, args: List[Value]):
        self.function_name = function_name
        self.arguments = args

    def __repr__(self):
        if len(self.arguments) > 0:
            return "(" + repr(self.function_name) + " " + " ".join(map(lambda x: repr(x), self.arguments)) + ")"
        return "(" + repr(self.function_name) + ")"


FunctionCase = Tuple[List[Value], Value]


class Function:
    def __init__(self, cases: List[FunctionCase]):
        self.cases = cases

    def is_constant(self):
        # TODO this check is not enough, it could not be constant even with a length of 1.
        # examples are boolean functions (see testsuite: quantified_permissions testcase: two_array_types)
        return len(self.cases) == 1

    def constant_value(self) -> str:
        if not self.is_constant():
            raise ValueError("Function is not a constant function.")
        return repr(self.cases[0][1])

    def __repr__(self):
        if self.is_constant():
            return self.constant_value()
        else:
            return "{" + "\n\t".join(map(lambda x: repr(x[0]) + " -> " + repr(x[1]), self.cases)) + "}"


class Model:
    def __init__(self, d: Dict[str, Function]):
        self.entries = d

    def __repr__(self):
        return "{\n" + "\n".join(starmap(lambda key, value: key + " : " + repr(value), self.entries.items())) + "\n}"

    def get_value(self, variable: str) -> Optional[str]:
        function = self.entries.get(variable)
        if function is not None and function.is_constant():
            return function.constant_value()
        return None

    def has_value(self, var: str) -> bool:
        return var in self.entries


class ModelParser(Parser):

    def __init__(self):
        self._model = ""
        self._pos = 0
        self._tokens = []
        self._token_index = 0

    @staticmethod
    def _parser_error_unexpected_string(found: str, expected: str):
        logging.error("Parser Error: Expected String '" + expected + "' but found '" + found + "'")
        exit(-1)

    @staticmethod
    def _parser_error(msg: str):
        logging.error("Parser Error: " + msg)
        exit(-1)

    def _parse_brackets(self) -> Value:
        self._consume_token("(")

        args = []
        while not self._check_token(")"):
            arg = self._parse_value()
            args.append(arg)

        self._consume_token(")")

        if len(args) > 1 and isinstance(args[0], Constant):
            return FuncApplication(args[0], args[1:])
        elif len(args) == 1:
            return args[0]
        else:
            self._parser_error("illegal use of brackets" + str(args))

    def _parse_value(self) -> Value:
        if self._check_token("("):
            return self._parse_brackets()
        else:
            token = self._next_token()
            value = Constant(token)

        return value

    def _parse_function_case(self) -> FunctionCase:
        if self._check_token("->"):
            self._parser_error_unexpected_string("->", "left side of a function-case")

        args = []
        while not self._check_token("->") and not self._check_token("}"):
            arg = self._parse_value()
            args.append(arg)

        if self._check_token("}"):
            if len(args) != 1:
                logging.error("Illegal state. expected constant function, but two values read.")
                exit(-1)

            return [Constant("else")], args[0]

        self._consume_token("->")

        result = self._parse_value()

        return args, result

    def _parse_function(self) -> Function:
        self._consume_token("{")

        cases = []
        while not self._check_token("}"):
            case = self._parse_function_case()
            if isinstance(case[1], Constant) and case[1].value == "#unspecified":
                continue
            cases.append(case)

        self._consume_token("}")
        return Function(cases)

    def _parse_single_model_entry(self) -> Tuple[str, Function]:
        name = self._next_token()

        self._consume_token("->")

        if self._check_token("{"):
            body = self._parse_function()
            return name, body
        else:
            value = self._parse_value()
            return name, Function([([Constant("else")], value)])

    def parse_model(self, model: str) -> Model:
        self._tokens = tokenizer.tokenize(model)
        self._model = model
        result = {}
        while self._token_index < len(self._tokens):
            name, function = self._parse_single_model_entry()
            result[name] = function
        return Model(result)


def get_model_from_z3_output(z3_output_file: Path) -> Model:
    input_lines = z3_output_file.read_text().split("\n")

    model_start_index = 0
    model_end_index = 0
    for i in range(0, len(input_lines)):
        if input_lines[i].startswith("\""):
            input_lines[i] = input_lines[i][1:]  # remove the starting "
            model_start_index = i

        if input_lines[i].endswith("\""):
            input_lines[i] = input_lines[i][:-1]  # remove the ending "
            model_end_index = i
            break

        input_lines[i] = input_lines[i]

    model_lines = input_lines[model_start_index: model_end_index + 1]
    model_lines = [s.strip() for s in model_lines]
    return ModelParser().parse_model(" ".join(model_lines))
