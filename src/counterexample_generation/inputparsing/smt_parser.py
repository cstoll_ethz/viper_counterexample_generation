from __future__ import annotations

from collections import OrderedDict
from json import JSONEncoder
from pathlib import Path
from typing import List, Dict, Set, Tuple, Optional, Iterable

from inputparsing import tokenizer
from inputparsing.parser import Parser
from inputparsing import smt_parser


class VariableDeclaration:
    def __init__(self, name: str, var_type: str):
        self.type = var_type
        self.name = name


class FunctionDeclaration:
    def __init__(self, name: str, param_types: Tuple[str], ret_type: str):
        self.return_type: str = ret_type
        self.name: str = name
        self.parameter_types: Tuple[str] = param_types


class Exp:

    def __init__(self):
        self.model_value: str = None
        self.smt_string: str = None

    def __eq__(self, other):
        if isinstance(other, type(self)):
            return self.smt_string == other.smt_string
        return NotImplemented

    def __ne__(self, other):
        return not self.__eq__(other)

    def __hash__(self):
        return hash(self.smt_string)

    def __repr__(self):
        return repr(self.smt_string)

    def assign_temp_variables_in_exp(self,
                                     assignment: Dict[str, smt_parser.Exp],
                                     func_calls: Dict[str, Set[smt_parser.FunctionApplication]] = {},
                                     new_calls: Set[smt_parser.FunctionApplication] = set()) -> smt_parser.Exp:
        if isinstance(self, smt_parser.Variable) or isinstance(self, smt_parser.Constant):
            if self.smt_string in assignment:
                return assignment[self.smt_string]
            else:
                return self
        elif isinstance(self, smt_parser.FunctionApplication):
            new_args = []
            for arg in self.args:
                new_args.append(arg.assign_temp_variables_in_exp(assignment, func_calls, new_calls))
            f_app = smt_parser.FunctionApplication(self.elements[0], self.func_declaration, tuple(new_args))
            if self.name in func_calls:
                if f_app not in func_calls[self.name]:
                    new_calls.add(f_app)
            else:
                new_calls.add(f_app)

            return f_app
        elif isinstance(self, smt_parser.BracketExpression):
            new_args = []
            for arg in self.elements:
                new_args.append(arg.assign_temp_variables_in_exp(assignment, func_calls, new_calls))
            return smt_parser.BracketExpression(tuple(new_args))


class Variable(Exp):

    def __init__(self, var: str):
        self.variable = var
        self.smt_string: str = var

    def contains(self, s: str):
        return s == self.smt_string

    def _input_string(self):
        return self.variable


class Constant(Exp):

    def __init__(self, value: str):
        self.smt_string: str = value
        self.model_value: str = value

    def contains(self, s: str):
        return s == self.smt_string

    def _input_string(self):
        return self.model_value


class BracketExpression(Exp):
    def __init__(self, sub_expr: Tuple[Exp]):
        self.elements: Tuple[Exp] = sub_expr
        self.smt_string: str = self._generate_smt_string()

    def __repr__(self):
        return repr(self.elements)

    def contains(self, s: str):
        for sub in self.elements:
            if sub.contains(s):
                return True
        return False

    def _generate_smt_string(self):
        return "({args})".format(args=" ".join([a.smt_string for a in self.elements]))


class FunctionApplication(BracketExpression):
    def __init__(self, name: Exp, f_decl: Optional[FunctionDeclaration], args: Tuple[Exp]):
        super().__init__(tuple([name] + list(args)))

        self.name: str = name.smt_string
        self.func_declaration: Optional[FunctionDeclaration] = f_decl
        self.args: List[Exp] = args
        self.model_value: str = None

    def __repr__(self):
        return "(" + repr(self.name) + " " + " ".join(map(lambda x: repr(x), self.args)) + ")"

    def contains(self, s: str):
        for sub in self.args:
            if sub.contains(s):
                return True
        return False


class ForAllExpression(BracketExpression):
    def __init__(self, forall: Constant,
                 var_bindings: BracketExpression,
                 annotation: Annotation):
        super().__init__(tuple([forall, var_bindings, annotation]))

        self.bindings: List[Tuple[Constant, Constant]] = [(item.elements[0], item.elements[1])
                                                          for item in var_bindings.elements]
        self.annotation = annotation


class Annotation(BracketExpression):
    def __init__(self, annotation: Exp, exp: Exp, smt_annotations: List[Exp]):
        super().__init__(tuple([annotation] + [exp] + smt_annotations))
        self.exp = exp

        if len(smt_annotations) % 2 != 0:
            raise ValueError("illegal number of smt annotations!")

        smt_ann: Dict[str, List[Exp]] = {}
        for name, value in zip(smt_annotations[0::2], smt_annotations[1::2]):
            if name.smt_string in smt_ann:
                smt_ann[name.smt_string] += [value]
            else:
                smt_ann[name.smt_string] = [value]

        self.smt_annotations: Dict[str, List[Exp]] = smt_ann


class SMTParser(Parser):

    def __init__(self, input_string: str):
        self.smt_string = input_string
        self._tokens = tokenizer.tokenize(input_string)
        self._token_index = 0
        self.variables: Dict[str, VariableDeclaration] = {}
        self.functions: Dict[str, FunctionDeclaration] = {}
        self.statements: List[BracketExpression] = None
        self.z3_functions = set(["<",
                                 ">",
                                 "=",
                                 "<=",
                                 ">=",
                                 "-",
                                 "+",
                                 "*",
                                 "/",
                                 "not",
                                 "implies",
                                 "and",
                                 "or",
                                 "$Snap.combine",
                                 "$Snap.first",
                                 "$Snap.second"])

        self.let_bindings: Dict[str, Exp] = {}

    def _add_function_declaration(self, tokens: List[Exp]):
        if len(tokens) != 4:
            raise ValueError(
                "wrong number of arguments in declare-fun (expected 4 but got " + str(len(tokens)) + "):" + repr(
                    tokens))
        symbol = tokens[1].smt_string
        arguments: Tuple[str] = tuple([t.smt_string for t in tokens[2].elements])
        return_type = tokens[3].smt_string

        self.functions[symbol] = FunctionDeclaration(symbol, arguments, return_type)

    def _add_const_declaration(self, tokens: List[Exp]):
        if len(tokens) != 3:
            raise ValueError(
                "wrong number of arguments in declare-const (expected 3 but got " + str(len(tokens)) + "):" + repr(
                    tokens))
        symbol = tokens[1].smt_string
        sort_type = tokens[2].smt_string

        self.variables[symbol] = VariableDeclaration(symbol, sort_type)

    def _parse_expression(self) -> Exp:
        if self._check_token("("):
            return self.parse_brackets()
        else:
            token = self._next_token()
            if token in self.variables:
                return Variable(token)
            return Constant(token)

    def parse_brackets(self) -> Optional[BracketExpression]:
        self._consume_token("(")

        args: List[Exp] = []
        while not self._check_token(")"):
            arg = self._parse_expression()
            if arg is not None:
                args.append(arg)

        self._consume_token(")")

        if len(args) == 0:
            return BracketExpression(tuple())
        elif len(args) == 1:
            if isinstance(args[0], Constant) and args[0].smt_string in self.functions \
                    and len(self.functions[args[0].smt_string].parameter_types) == 0:
                # (const_function)
                decl = self.functions[args[0].smt_string]
                return FunctionApplication(args[0], decl, tuple())
            else:
                return BracketExpression(tuple([args[0]]))
        else:
            if isinstance(args[0], Constant):
                if args[0].smt_string == "!":
                    return Annotation(args[0], args[1], args[2:])
                elif args[0].smt_string == "declare-fun":
                    self._add_function_declaration(args)
                    return None
                elif args[0].smt_string == "declare-const":
                    self._add_const_declaration(args)
                    return None
                elif args[0].smt_string == "let":
                    return args[len(args) - 1]
                elif args[0].smt_string == "forall":
                    bindings: BracketExpression = args[1]
                    exp = args[2]
                    return ForAllExpression(args[0], bindings, exp)
                elif args[0].smt_string in self.functions \
                        and len(self.functions[args[0].smt_string].parameter_types) == len(args[1:]):
                    decl = self.functions[args[0].smt_string]
                    return FunctionApplication(args[0], decl, tuple(args[1:]))
                elif args[0].smt_string in self.z3_functions:
                    return FunctionApplication(args[0], None, tuple(args[1:]))

            return BracketExpression(tuple(args))

    def parse_statements(self):
        statements: List[BracketExpression] = []
        while self._check_token("("):
            s = self.parse_brackets()
            if s is not None:
                statements.append(s)

        if self._token_index != len(self._tokens):
            raise ValueError("did not parse: '{remainder}'".format(remainder=" ".join(self._tokens)))

        self.statements = statements


class SMTFile:
    def __init__(self,
                 smt_query: str,
                 var_decl: Dict[str, VariableDeclaration],
                 fun_decl: Dict[str, FunctionDeclaration],
                 statements: List[Exp],
                 call_dict: Dict[str, Set[FunctionApplication]]):
        self.smt_query = smt_query
        self.variable_declarations: Dict[str, VariableDeclaration] = var_decl
        self.function_declarations: Dict[str, FunctionDeclaration] = fun_decl
        self.statements: List[Exp] = statements
        self.call_dict: OrderedDict[str, Set[FunctionApplication]] = call_dict


def _create_smt_parser(content: List[str]) -> SMTParser:
    smt_content = ""

    # remove comments
    for line in content:
        for t in tokenizer.tokenize(line, single_char_tokens=["(", ")", ";"]):
            if t == ";":
                break
            smt_content += t + " "

    parser = SMTParser(smt_content)
    parser.parse_statements()
    return parser


def _build_function_application_dictionary(expressions: Iterable[BracketExpression],
                                           calls: OrderedDict[str, Set[FunctionApplication]],
                                           temporary_variables: List[str] = []) \
        -> Dict[str, Set[FunctionApplication]]:
    for e in expressions:

        if isinstance(e, FunctionApplication):
            if any(e.contains(temp) for temp in temporary_variables):
                continue

            if isinstance(e.name, str):
                if e.name in calls:
                    calls[e.name].add(e)
                else:
                    calls[e.name] = {e}

        if isinstance(e, BracketExpression) and e.elements is not None:
            recursive_list: Iterable[BracketExpression] = filter(
                lambda s: isinstance(s, BracketExpression), e.elements)

            # TODO if "let" would be handled correctly, we can remove "let" from this list
            if len(e.elements) > 0 and e.elements[0].smt_string in ["forall", "exists", "let"]:
                binding_list = e.elements[1].elements
            elif len(e.elements) > 0 and e.elements[0].smt_string in ["match", "define-fun"]:
                binding_list = e.elements[2].elements
            else:
                binding_list = []

            temp_vars = [binding.elements[0].smt_string for binding in binding_list]
            result = _build_function_application_dictionary(recursive_list, OrderedDict(),
                                                            temporary_variables + temp_vars)
            for decl, found_calls in result.items():
                if decl in calls:
                    calls[decl] = calls[decl].union(found_calls)
                else:
                    calls[decl] = found_calls

    return calls


def parse_smt_file(smt_file: Path) -> SMTFile:
    smt_content = smt_file.read_text()
    lines = smt_content.split("\n")

    parser: SMTParser = _create_smt_parser(lines)
    calls = _build_function_application_dictionary(parser.statements, OrderedDict())

    return SMTFile(smt_content, parser.variables, parser.functions, parser.statements, calls)


def parse_smt_expression(expression: str) -> Exp:
    parser = SMTParser(expression)
    parser.parse_statements()

    return parser.statements[0]
