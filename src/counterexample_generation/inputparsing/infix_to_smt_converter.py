import logging
from typing import List, Tuple

import inputparsing.smt_parser as smt_parser
import inputparsing.tokenizer as Tokenizer


def _translate_function_name_to_smt2(name: str) -> str:
    return name.replace("[", "<").replace("]", ">")


def infix_to_smt(infix: str) -> smt_parser.Exp:
    operators = [",", "*", "/", "-", "+", "&&", "||", "<", "<=", "=", ">", ">=", "==>", "in"]

    function_mapping: Dict[str, str] = {"&&": "and",
                                        "||": "or",
                                        "==>": "implies",
                                        "in": "Set_in"}

    highest_precedence = 100
    # define a precedence dictionary
    prec = {}
    prec['*'] = 11
    prec['/'] = 11
    prec['-'] = 10
    prec['+'] = 10

    prec['<'] = 5
    prec['<='] = 5
    prec['='] = 5
    prec['>='] = 5
    prec['>'] = 5

    prec["in"] = 5

    prec['&&'] = 4
    prec['||'] = 4

    prec['==>'] = 3
    prec['('] = 2
    prec[')'] = 1
    prec[','] = 0

    tokens = Tokenizer.tokenize(infix)

    params = ["(", ")"]

    func_ap = "^func_appl^"

    logging.debug(infix)
    operator_stack: List[str] = []
    operand_stack: List[smt_parser.Exp] = []

    def precedence(symbol: str) -> int:
        if symbol in prec:
            return prec[symbol]
        return highest_precedence

    def apply_operator():
        _op = operator_stack[-1]

        _operands = [operand_stack.pop()]
        while len(operator_stack) > 0 and operator_stack[-1] == _op:
            _operands.append(operand_stack.pop())
            operator_stack.pop()

        if _op in function_mapping:
            _op = function_mapping[_op]

        _op = _translate_function_name_to_smt2(_op)
        _name = smt_parser.Constant(_op)

        _operands.reverse()
        _exp = smt_parser.FunctionApplication(_name, f_decl=None, args=tuple(_operands))
        operand_stack.append(_exp)

    last_token = "||"
    for token in tokens:
        if token in operators or token in params:
            if token == '(':
                if last_token in operators:
                    operator_stack.append(token)
                else:
                    f = operand_stack.pop()
                    operator_stack.append(f.smt_string)
                    operator_stack.append(func_ap)
                    operator_stack.append(token)

            elif len(operator_stack) == 0 or (precedence(token) >= precedence(operator_stack[-1])):
                operator_stack.append(token)
            elif token == ')':

                while operator_stack[-1] != "(" and operator_stack[-1] != ",":
                    apply_operator()

                operands = []
                if operator_stack[-1] == ",":
                    while operator_stack[-1] != "(":
                        operands.append(operand_stack.pop())
                        operator_stack.pop()

                if operator_stack.pop() != "(":
                    raise ValueError("expected a '(' on the stack")

                if operator_stack[-1] == func_ap:
                    operands.append(operand_stack.pop())

                    operator_stack.pop()  # pop func_ap token
                    f = operator_stack.pop()
                    f = _translate_function_name_to_smt2(f)
                    name = smt_parser.Constant(f)
                    operands.reverse()
                    exp = smt_parser.FunctionApplication(name, f_decl=None, args=tuple(operands))
                    operand_stack.append(exp)

            elif prec[token] <= prec[operator_stack[-1]]:
                while len(operator_stack) > 0 and precedence(token) <= precedence(operator_stack[-1]) \
                        and (operator_stack[-1] != "," and operator_stack[-1] != "("):
                    apply_operator()

                operator_stack.append(token)
        else:
            # TODO decide if it is a constant or variable
            c = smt_parser.Constant(token)
            operand_stack.append(c)

        last_token = token

    while len(operator_stack) > 0:
        apply_operator()

    if len(operand_stack) != 1:
        raise ValueError("error during infix to smt conversion! " + str(operand_stack))

    return operand_stack[-1]
