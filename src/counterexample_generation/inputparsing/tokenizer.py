from typing import List


class _Tokenizer:

    def __init__(self, input_string: str, key_characters: List[str], string_quote: str, quote_escape: str):
        self._input_string = input_string
        self._key_characters = key_characters
        self._string_quote = string_quote
        self._quote_escape = quote_escape
        self._pos = 0

    def _pos_valid(self) -> bool:
        return 0 <= self._pos < len(self._input_string)

    def _current_char(self) -> str:
        if not self._pos_valid():
            raise ValueError("position is out of bounds!\n{smt}".format(smt=self._input_string))
        return self._input_string[self._pos]

    def _n_next_characters(self, n: int) -> str:
        if self._pos < 0 or self._pos >= len(self._input_string):
            raise ValueError("position is out of bounds!\n{smt}".format(smt=self._input_string))
        return self._input_string[self._pos + 1:min(self._pos + n, len(self._input_string))]

    def _skip_whitespace(self):
        while self._pos_valid() and self._current_char().isspace():
            self._pos += 1

    def _parse_string_token(self) -> str:
        token = "\""
        # skip first "
        self._pos += 1

        len_escape = len(self._quote_escape)

        while self._current_char() != self._string_quote \
                or (self._current_char() == self._quote_escape[0]
                    and self._n_next_characters(len_escape - 1) == self._quote_escape[1:]):
            token += self._current_char()
            if self._current_char() == self._string_quote:
                self._pos += len_escape
            else:
                self._pos += 1

        token += "\""
        # skip last "
        self._pos += 1

        return token

    def get_token(self) -> str:
        self._skip_whitespace()

        if 0 > self._pos or self._pos >= len(self._input_string):
            return ""

        if self._current_char() == self._string_quote:
            return self._parse_string_token()

        token = self._current_char()
        self._pos += 1

        while self._pos < len(self._input_string) \
                and not self._current_char().isspace() \
                and self._current_char() not in self._key_characters \
                and token not in self._key_characters:
            token += self._current_char()
            self._pos += 1

        return token


def tokenize(input_str: str, single_char_tokens: List[str] = ["(", ")"], string_quote: str = "\"",
             quote_escape_string: str = "\"\"") -> List[str]:
    """
    Creates a list of tokens from a string.

    :param input_str: The string to tokenize
    :param single_char_tokens: Key characters that are a single token (outside of string literals)
                           examples are "(", ")", ";", ...
    :param string_quote: String quote character, default "
    :param quote_escape_string: How is " escaped in a string literal, common are "" or \"
    :return: List[str] of all the Tokens in input_str.
    """
    tokenizer: _Tokenizer = _Tokenizer(input_str, single_char_tokens, string_quote, quote_escape_string)
    tokens: List[str] = []
    for token in iter(tokenizer.get_token, ""):
        tokens.append(token)
    return tokens
