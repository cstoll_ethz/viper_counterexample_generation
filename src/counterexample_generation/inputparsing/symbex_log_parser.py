import logging
from typing import List, Dict, Tuple
from pathlib import Path
import json


class SymbexLogHeap:

    class Permission:
        # "HEAP_CHUNK.FIELD -> VALUE # PERM_AMOUNT"
        def __init__(self, heap_chunk: str, field: str, value: str, perm_amount: str):
            self.heap_chunk = heap_chunk
            self.field = field
            self.value = value
            self.perm_amount = perm_amount

    class QuantifiedPermission:
        # "QA VAR :: VAR.FIELD -> VALUE # PERM_AMOUNT"
        def __init__(self, var: str, field: str, value: str, perm_amount: str):
            self.field = field
            self.value = value
            self.perm_amount = perm_amount
            self.variable = var

    class PredicateInstance:
        # "PREDICATE(SNAPSHOT; ARGUMENTS)"
        def __init__(self, predicate: str, snapshot: str, arguments: List[str]):
            self.predicate = predicate
            self.snapshot = snapshot
            self.arguments = arguments

    class OtherHeapEntry:
        # "ENTRY"
        def __init__(self, entry: str):
            self.entry = entry

    def __init__(self, perm: List[Permission], qp: List[QuantifiedPermission], pred: List[PredicateInstance],
                 other: List[OtherHeapEntry]):
        self.permissions = perm
        self.quantified_permissions = qp
        self.predicate_instances = pred
        self.others = other


def get_heap(last_entry) -> SymbexLogHeap:
    heap_data = last_entry["heap"]
    return _parse_heap(heap_data)


def get_store(last_entry) -> Dict[str, str]:
    store_data = last_entry["store"]

    result = {}
    for entry in store_data:
        value = entry["value"]
        t = entry["type"]

        # value string has the form of "b -> b@1@01"
        assignments = value.split("->")
        viper_variable = assignments[0].strip()
        z3_constant = assignments[1].strip()

        result[viper_variable] = (z3_constant, t)

    return result


def parse(symbex_log_file: Path, failing_function: str) -> Tuple[SymbexLogHeap, Dict[str, str]]:
    last_entry = _get_last_entry_from_symbexlog(symbex_log_file, failing_function)
    return get_heap(last_entry), get_store(last_entry)


def _get_last_entry_from_symbexlog(symbex_log_file: Path, failing_entity: str):
    content_str = symbex_log_file.read_text()

    content = json.loads(content_str)
    for entry in content:
        if entry["value"] == failing_entity:
            break

    if entry["value"] != failing_entity:
        logging.error("could not find the failing entity in the symbolic execution log.")
        exit(-1)

    if entry["children"] and len(entry["children"]) > 0:
        last_action = entry["children"][len(entry["children"]) - 1]
        if last_action["prestate"]:
            return last_action["prestate"]

    return []


def _parse_heap(heap_list: List[str]) -> SymbexLogHeap:

    predicate_instances = [entry for entry in heap_list if "->" not in entry]
    ref_field_entries = [entry for entry in heap_list if "->" in entry]

    other_entries = []
    permissions = []
    quantified_permissions = []
    predicates = []

    for element in predicate_instances:
        (predicate, arg_list) = element.split("(")
        arg_list = arg_list.split("#")[0].strip()
        snapshot, arguments = arg_list.split(";")
        arguments = arguments[:-1]  # remove the last ")"

        arg_list = arguments.split(",")
        predicates.append(SymbexLogHeap.PredicateInstance(predicate, snapshot, arg_list))

    for element in ref_field_entries:
        (ref_field, heap_value) = element.split("->")

        # remove the permission amount after #
        right_side = heap_value.split("#")
        value = right_side[0].strip()
        permission_amount = right_side[1].strip()

        (variable, field) = ref_field.strip().split(".")
        field = field.strip()
        if " " in variable:
            # Quantified Chunk
            if variable.startswith("QA"):
                var = variable.split(" ")[1].strip()
                qp = SymbexLogHeap.QuantifiedPermission(var, field, value, permission_amount)
                quantified_permissions.append(qp)
            else:
                # unknown case
                other_entries.append(element)
        else:
            # simple REF_VARIABLE.FIELD case
            p = SymbexLogHeap.Permission(variable, field, value, permission_amount)
            permissions.append(p)

    return SymbexLogHeap(permissions, quantified_permissions, predicates, other_entries)
