from json import JSONDecoder, JSONDecodeError
import json
import sys
from typing import List, Tuple, Optional, Any
import re
from pathlib import Path

NOT_WHITESPACE = re.compile(r'[^\s]')


def _decode_stacked(document, pos=0, decoder=JSONDecoder()):
    while True:
        match = NOT_WHITESPACE.search(document, pos)
        if not match:
            return
        pos = match.start()

        try:
            obj, pos = decoder.raw_decode(document, pos)
        except JSONDecodeError:
            raise
        yield obj


def _get_error(msg_body) -> dict:
    if msg_body["status"] != "failure" or msg_body["kind"] != "for_entity":
        return {}

    return msg_body["details"]


def _get_failing_method(error) -> str:
    return error["entity"]["name"]


def get_failing_methods(record) -> Optional[Tuple[str, Any]]:

    if "msg_type" in record and record["msg_type"] == "verification_result":
        error = _get_error(record["msg_body"])
        if error:
            return _get_failing_method(error), json.dumps(error)

    return None, None


def get_summary(record):
    if "msg_type" in record and record["msg_type"] == "verification_result" and record["msg_body"]["kind"] == "overall":
        errors = record["msg_body"]["details"]
        if errors:
            return json.dumps(errors)
    return None


def parse(viper_client_log: Path):
    if not viper_client_log.is_file():
        raise ValueError("input_file was not a file or output_dir was not a directory!")

    failing_methods = []
    summary = None
    with open(viper_client_log, "r") as logFile:
        content = logFile.read()
        for record in _decode_stacked(content):
            fail_method, error = get_failing_methods(record)
            if fail_method is not None:
                failing_methods.append((fail_method, error))

            s = get_summary(record)
            if s is not None:
                summary = s

    return failing_methods, summary
