

class Parser:
    def __init__(self):
        self._tokens = []
        self._token_index = 0

    def _check_token(self, token: str):
        return 0 <= self._token_index < len(self._tokens) and self._tokens[self._token_index] == token

    def _consume_token(self, token: str):
        if self._token_index < 0 or self._token_index >= len(self._tokens):
            raise ValueError("Error: expected {exp} but no token left.".format(exp=token))
        if self._tokens[self._token_index] != token:
            raise ValueError("Error: expected {exp} but found {found}.".
                             format(exp=token,
                                    found=self._tokens[self._token_index]))
        self._token_index += 1

    def _next_token(self):
        token = self._tokens[self._token_index]
        self._token_index += 1
        return token
