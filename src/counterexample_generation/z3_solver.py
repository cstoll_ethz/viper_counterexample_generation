import random
import subprocess
from queue import Queue, Empty
import threading
import time
from typing import List, Dict
import logging

import inputparsing.smt_parser as smt_parser
from inputparsing.z3_model_parser import Model, ModelParser


class AsynchronousFileReader(threading.Thread):
    # from https://www.stefaanlippens.net/python-asynchronous-subprocess-pipe-reading/
    """
    Helper class to implement asynchronous reading of a file/pipe
    in a separate thread. Pushes read lines on a queue to
    be consumed in another thread.
    """

    def __init__(self, fd, queue):
        assert isinstance(queue, Queue)
        assert callable(fd.readline)
        threading.Thread.__init__(self)
        self._fd = fd
        self._queue = queue

    def run(self):
        """The body of the tread: read lines and put them on the queue."""
        for line in iter(self._fd.readline, ''):
            self._queue.put(line)

    def eof(self):
        """Check whether there is no more content to expect."""
        return not self.is_alive() and self._queue.empty()


class Z3Solver:

    def __init__(self, debug_name="", inverse_expression_dict: Dict[str, smt_parser.Exp] = {},
                 store_inverse_expression: bool = False):
        self._logger = logging.getLogger("Z3Solver_" + debug_name)
        self._debug_name = debug_name
        self._z3_exe = "/usr/local/Viper/z3/bin/z3"
        # , "model.completion=true"
        self._arguments = ["pp.single_line=true", "model.partial=true", "model.completion=true",
                           "pp.min_alias_size=4294967295", "pp.max-depth=4294967295", "model.v2=true", "-smt2", "-in"]
        self._process = None
        self._reader: AsynchronousFileReader = None
        self._queue: Queue[str] = None

        self._inverse_expression_dict: Dict[str, smt_parser.Exp] = inverse_expression_dict
        self._store_inverse_expression: bool = store_inverse_expression

        self.start()

    def start(self):
        self._process: subprocess.Popen = subprocess.Popen([self._z3_exe] + self._arguments, stdout=subprocess.PIPE,
                                                           stdin=subprocess.PIPE,
                                                           stderr=subprocess.STDOUT,
                                                           encoding='utf8')
        self._queue = Queue()
        self._reader = AsynchronousFileReader(self._process.stdout, self._queue)
        self._reader.start()

    def stop(self):
        self._logger.info("stop()")
        self.send("(exit)")

        counter = 0
        while not self._reader.eof() and counter < 5:
            try:
                self._queue.get(block=False)
            except Empty:
                time.sleep(0.2)
                counter += 1
                self._logger.info("Wait timer z3 shutdown: " + str(counter))

        self._process.stdin.close()
        self._process.stdout.close()

        self._process.terminate()
        self._process.wait()

        self._process = None

        self._logger.info("Z3 shutdown")

    def __del__(self):
        if self._process is not None:
            self._logger.debug("__del__")
            self.stop()

    def _generate_random_token(self) -> str:
        # sends '(eval "z3_init: randomInt")' to Z3 and checks its responses until we see the random string.
        # With this simple test we know that Z3 is ready and waits for further requests
        rand_int = random.randint(10 ** 6, 10 ** 8)
        return "\"z3_init: {rand}\"".format(rand=rand_int)

    def init_session(self, smt_query: str):

        init_token = self._generate_random_token()
        self._logger.debug(init_token)
        self.send(smt_query, init_token)
        self.read_multiline_response(init_token)
        # cleared queue and no further messages should come from z3
        self.send("(get-model)", init_token)
        self._logger.debug(self.read_multiline_response(init_token))
        self.send("(eval ($FVF.lookup_val (as sm@10@01 $FVF<Int>) (loc<Ref> a@2@01 0)))")
        self._logger.debug(self.read_response())

        self._logger.info("Z3 initialization completed!")

    def read_multiline_response(self, init_token: str) -> List[str]:
        responses = []
        z3_ready = False
        while not z3_ready and not self._reader.eof():
            response = self._queue.get(block=True)
            response = response[:-1]  # remove the '\n'
            self._logger.debug("read: " + response)

            z3_ready = response == init_token
            if not z3_ready:
                responses.append(response)

        if self._reader.eof():
            raise Exception("Z3 crashed during evaluation of the last query!")

        return responses

    def send(self, msg, multiline_token=""):
        if not msg.endswith("\n"):
            msg += "\n"
        if multiline_token != "":
            eval_token = "(eval {token})\n".format(token=multiline_token)
        else:
            eval_token = ""
        msg_to_send = msg + eval_token
        self._logger.debug("Send: " + msg[:-1])
        self._process.stdin.write(msg_to_send)
        self._process.stdin.flush()

    def read_response(self):
        response: str = self._queue.get()
        response = response[:-1].strip()  # remove the '\n'
        self._logger.debug("Response: " + response)
        if response.startswith("(error"):
            return None
        return response

    def successful(self) -> bool:
        return self.read_response() == "success"

    def check_equality(self, x: str, y: str) -> bool:
        response = self.eval("(= {x} {y})".format(x=x, y=y))
        return response == "true"

    def eval(self, expression: str) -> str:
        statement = "(eval {exp})".format(exp=expression)
        self.send(statement)

        response = self.read_response()

        if self._store_inverse_expression and response not in self._inverse_expression_dict:
            self._inverse_expression_dict[response] = smt_parser.parse_smt_expression(expression)
        return response

    def check_sat(self) -> bool:
        self.send("(check-sat)")
        response = self.read_response()
        return response == "sat"

    def get_model(self) -> Model:
        token = self._generate_random_token()
        self.send("(get-model)", token)
        response = self.read_multiline_response(token)
        response = "\n".join(response)
        self._logger.debug("Get Model Response: " + response)
        response = response[1:][:-1]
        parser = ModelParser()
        return parser.parse_model(response)
