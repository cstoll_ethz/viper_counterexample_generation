from typing import List, Iterable, Dict, Tuple, Set, Optional

from inputparsing import smt_parser
from matching_trigger_candidates import CandidateMatching
from z3_solver import Z3Solver


def search_all_forall_expressions(expressions: Iterable[smt_parser.Exp], solver: Z3Solver) \
        -> List[smt_parser.ForAllExpression]:
    result: List[smt_parser.ForAllExpression] = []

    for exp in expressions:
        if isinstance(exp, smt_parser.ForAllExpression):
            # if solver.check_if_valid(exp.input_string):
            result.append(exp)
        elif isinstance(exp, smt_parser.BracketExpression):
            result += search_all_forall_expressions(exp.elements, solver)

    return result


def _merge_assignment_dict(d1: Dict[str, smt_parser.Exp], d2: Dict[str, smt_parser.Exp]) \
        -> Tuple[bool, Dict[str, smt_parser.Exp]]:
    if d2 is None:
        return True, d1

    for name, value in d2.items():
        if not _update_assignment_dict(d1, name, value):
            return False, {}
    return True, d1


def _update_assignment_dict(d: Dict[str, smt_parser.Exp], var: str, exp: smt_parser.Exp) -> bool:
    if var in d:
        return d[var] == exp
    else:
        d[var] = exp
        return True


def _match_function_call_to_trigger(trigger: smt_parser.FunctionApplication,
                                    f: smt_parser.FunctionApplication,
                                    var_bindings: Set[str],
                                    solver: Z3Solver) -> Optional[Dict[str, smt_parser.Exp]]:
    if trigger.name != f.name:
        return None

    assignments = {}

    for t_arg, f_arg in zip(trigger.args, f.args):

        # t_arg is a bound variable
        if t_arg.smt_string in var_bindings:
            if not _update_assignment_dict(assignments, t_arg.smt_string, f_arg):
                return None

        # t_arg is a function call
        elif isinstance(t_arg, smt_parser.FunctionApplication):
            if isinstance(f_arg, smt_parser.FunctionApplication):
                sub_dict = _match_function_call_to_trigger(t_arg, f_arg, var_bindings, solver)
                if sub_dict is None:
                    return None
                successful, assignments = _merge_assignment_dict(assignments, sub_dict)
                if not successful:
                    return None
            else:
                return None

        # t_arg is a constant, variable, ...
        else:
            if not solver.check_equality(t_arg.smt_string, f_arg.smt_string):
                return None

    return assignments


def _find_matching_function_calls(trigger: smt_parser.FunctionApplication,
                                  func_calls: Dict[str, Set[smt_parser.FunctionApplication]],
                                  var_bindings: Set[str],
                                  solver: Z3Solver) \
        -> List[Dict[str, smt_parser.Exp]]:
    if trigger.name not in func_calls:
        return []

    calls = func_calls[trigger.name]
    assignments: List[Dict[str, smt_parser.Exp]] = []
    for f_call in calls:
        mapping = _match_function_call_to_trigger(trigger, f_call, var_bindings, solver)
        if mapping is not None:
            assignments.append(mapping)

    return assignments


def _instantiate_forall_expression_with_assignments(exp: smt_parser.Exp,
                                                    assignments: List[Dict[str, smt_parser.Exp]],
                                                    func_calls: Dict[str, Set[smt_parser.FunctionApplication]]) \
        -> Set[smt_parser.FunctionApplication]:
    result = set()
    for assign in assignments:
        new_calls = set()
        exp.assign_temp_variables_in_exp(assign, func_calls, new_calls)

        if len(new_calls) > 0:
            result = result.union(new_calls)

    return result


def instantiate_forall_expressions(func_calls: Dict[str, Set[smt_parser.FunctionApplication]],
                                   foralls: List[smt_parser.ForAllExpression],
                                   solver: Z3Solver) -> Set[smt_parser.FunctionApplication]:

    match_engine: CandidateMatching[smt_parser.Exp] = CandidateMatching()

    new_functions = set()

    for forall in foralls:
        patterns: List[smt_parser.Exp] = forall.annotation.smt_annotations[":pattern"]
        bound_variables = set([b[0].smt_string for b in forall.bindings])

        possible_assignments: List[Dict[str, smt_parser.Exp]] = []
        for pattern in patterns:
            if not isinstance(pattern, smt_parser.BracketExpression):
                raise ValueError("Expected a bracket expression as a pattern!")

            matching_calls: List[List[Dict[str, smt_parser.Exp]]] = []

            # two cases, the pattern is just a single function call or a bracket expression with calls
            if isinstance(pattern, smt_parser.FunctionApplication):
                matching_calls.append(_find_matching_function_calls(pattern, func_calls, bound_variables, solver))

            elif isinstance(pattern, smt_parser.BracketExpression):
                for single_call in pattern.elements:
                    if not isinstance(single_call, smt_parser.FunctionApplication):
                        raise ValueError("Expected a function call inside a pattern! " + single_call.smt_string)
                    matching_calls.append(
                        _find_matching_function_calls(single_call, func_calls, bound_variables, solver))

            if any([len(matches) == 0 for matches in matching_calls]):
                # for one trigger we found no matches -> no instantiations were made
                continue

            solutions = match_engine.get_all_matching_assignments(candidates=matching_calls)
            possible_assignments.extend(solutions)

        if len(possible_assignments) == 0:
            continue

        result: Set[smt_parser.FunctionApplication] = _instantiate_forall_expression_with_assignments(
            forall.annotation.exp, possible_assignments, func_calls)

        new_functions = new_functions.union(result)

    return new_functions
