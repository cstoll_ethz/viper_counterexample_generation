from __future__ import annotations

import logging
from typing import Dict, List, Optional, Iterable, Set

import counterexample
import ordered_set
from forall_instantiations import instantiate_forall_expressions, search_all_forall_expressions
from z3_solver import Z3Solver
from inputparsing import smt_parser
from inputparsing.z3_model_parser import Model


class FuncGraph:
    class Node:
        _id = 0

        def __init__(self, val_type: str):
            self.id: int = FuncGraph.Node._id
            self.type: str = val_type
            self.in_nodes: List[FuncGraph.Node] = []
            self.out_nodes: List[FuncGraph.Node] = []
            FuncGraph.Node._id += 1

        def display_value(self) -> str:
            return str(self.id)

    class ValueNode(Node):
        def __init__(self, value: str, val_type: str):
            super().__init__(val_type)
            self.value: str = value

        def display_value(self) -> str:
            return self.value

        def __str__(self):
            return self.value

    class ResultNode(ValueNode):
        def __init__(self, value: str, val_type: str):
            super().__init__(value, val_type)

    class VariableNode(ValueNode):
        def __init__(self, var: str, value: str, val_type: str):
            super().__init__(value, val_type)
            self.variable = var

        def display_value(self) -> str:
            if self.type is not None:
                return self.variable + ": " + self.type + " = " + self.value
            else:
                return self.variable + " = " + self.value

        def __str__(self):
            return self.variable + ": " + self.value

    class FunctionNode(Node):
        def __init__(self, func: str, arguments: List[FuncGraph.Node], result_node: FuncGraph.ResultNode):
            super().__init__(result_node.type)
            self.function: str = func
            self.in_nodes: List[FuncGraph.Node] = arguments
            for arg in arguments:
                arg.out_nodes.append(self)
            self.result_node: FuncGraph.ResultNode = result_node

            self.out_nodes.append(result_node)
            result_node.in_nodes.append(self)

        def display_value(self) -> str:
            return self.function

        def __str__(self):
            return self.function

    def __init__(self, ce: counterexample.CE):
        self.nodes: List[FuncGraph.Node] = []
        self.variable_nodes: Dict[str, FuncGraph.VariableNode] = {}
        self.node_cache: Dict[str, FuncGraph.Node] = {}

        self.ce: counterexample.CE = ce

    def _handle_bracket_expression(self, b_exp: smt_parser.BracketExpression, exp_type: str, z3_model: Model,
                                   solver: Z3Solver) -> Optional[Node]:
        if b_exp.elements[0].smt_string == "as":
            return self._handle_single_argument(b_exp.elements[1], exp_type, z3_model, solver)
        else:
            logging.warning("unknown bracket expression: " + b_exp.smt_string)
            return self._build_subgraph_from_unknown_bracket_expression(b_exp, exp_type, z3_model, solver)

    def _handle_single_argument(self,
                                arg: smt_parser.Exp,
                                arg_type: Optional[str],
                                z3_model: Model,
                                solver: Z3Solver) \
            -> Optional[Node]:
        new_node = None
        if isinstance(arg, smt_parser.FunctionApplication):
            f_node = self.build_subgraph_from_function_call(arg, z3_model, solver)
            if f_node is not None:
                new_node = f_node.result_node
        elif isinstance(arg, smt_parser.Variable) and z3_model.has_value(arg.smt_string):
            # arg is a symbolic value (variable in the smt file)
            if arg.smt_string in self.node_cache:
                var_node = self.node_cache[arg.smt_string]
            else:
                arg.model_value = z3_model.get_value(arg.smt_string)
                decl: smt_parser.VariableDeclaration = self.ce.sources.smt_file.variable_declarations[arg.smt_string]

                if arg_type is not None and decl.type != arg_type:
                    # raise ValueError(
                    logging.warning(
                        "Variable type is not equal to the argument type! Arg_type: " + arg_type + ": decl: "
                        + decl.type + " Arg: " + arg.smt_string + ". Skipped this node!")
                    return None

                var_node = FuncGraph.VariableNode(arg.smt_string, arg.model_value, decl.type)

                self.nodes.append(var_node)
                self.node_cache[arg.smt_string] = var_node

            new_node = var_node
        elif isinstance(arg, smt_parser.Variable) and not z3_model.has_value(arg.smt_string):
            logging.info("variable without model detected:" + arg.smt_string)
        elif isinstance(arg, smt_parser.BracketExpression):
            new_node = self._handle_bracket_expression(arg, arg_type, z3_model, solver)
            if new_node is not None and isinstance(new_node, FuncGraph.FunctionNode):
                new_node = new_node.result_node
        elif isinstance(arg, smt_parser.Constant):
            node = FuncGraph.ValueNode(arg.smt_string, arg_type)
            self.nodes.append(node)
            new_node = node

        return new_node

    def _build_subgraph(self, b_exp: smt_parser.BracketExpression,
                        types: List[Optional[str]],
                        exp_type: Optional[str],
                        z3_model: Model,
                        solver: Z3Solver) -> Optional[Node]:

        if types is not None and b_exp is not None and len(b_exp.elements) - 1 != len(types):
            logging.info("len b_exp.elements-1: " + str(len(b_exp.elements[1:])) + " len types: " + str(len(types)))
            raise ValueError(
                "bracket expression arguments were not equal to the number of types!\n" + b_exp.smt_string)

        # caching
        if b_exp.smt_string in self.node_cache:
            return self.node_cache[b_exp.smt_string]

        if types is None:
            types = [None] * len(b_exp.elements)

        b_exp.model_value = solver.eval(b_exp.smt_string)

        if b_exp.model_value is None:
            # Z3 could not evaluate this call, skip this function application
            # (maybe some variable was only a temporary variable)
            logging.info("Failed to evaluate: " + b_exp.smt_string)
            return None

        arg_nodes = []

        f_name: str = b_exp.elements[0].smt_string
        zipped = list(zip(b_exp.elements[1:], types))

        # do not include %limited functions
        if f_name.endswith("%limited"):
            return None

        for arg, arg_type in zipped:
            new_node = self._handle_single_argument(arg, arg_type, z3_model, solver)

            if new_node is not None:
                arg_nodes.append(new_node)
            else:
                # Argument is not determined in the current model by Z3
                # skip this subgraph
                return None

        result_node = FuncGraph.ResultNode(b_exp.model_value, exp_type)
        self.nodes.append(result_node)

        f_node = FuncGraph.FunctionNode(f_name, arg_nodes, result_node)
        self.nodes.append(f_node)

        self.node_cache[b_exp.smt_string] = f_node
        return f_node

    def _build_subgraph_from_unknown_bracket_expression(self, b_exp: smt_parser.BracketExpression,
                                                        exp_type: str,
                                                        z3_model: Model,
                                                        solver: Z3Solver) -> Optional[Node]:

        types = [exp_type] * (len(b_exp.elements) - 1)
        return self._build_subgraph(b_exp, types, exp_type, z3_model, solver)

    def build_subgraph_from_function_call(self, call: smt_parser.FunctionApplication,
                                          z3_model: Model, solver: Z3Solver) -> \
            Optional[FuncGraph.FunctionNode]:

        if call.func_declaration is None:
            types = None
            ret_type = None
        else:
            types = list(call.func_declaration.parameter_types)
            ret_type = call.func_declaration.return_type

        return self._build_subgraph(call, types, ret_type, z3_model, solver)

    def remove_node(self, node: Node):
        self.nodes.remove(node)

        for in_node in node.in_nodes:
            in_node.out_nodes.remove(node)

        for out_node in node.out_nodes:
            out_node.in_nodes.remove(node)

    def remove_and_reconnect_neighbors(self, node: Node):

        for in_node in node.in_nodes:
            for out_node in node.out_nodes:
                in_node.out_nodes.append(out_node)
                out_node.in_nodes.append(in_node)

        self.remove_node(node)

    def simplify_graph(self):
        # remove the return value nodes in between function calls e.g. f(g(2)) we dont need the return value for g(2)
        for node in self.nodes:
            if isinstance(node, FuncGraph.FunctionNode) and len(node.out_nodes) == 1 \
                    and len(node.out_nodes[0].out_nodes) > 0 \
                    and all(isinstance(n, FuncGraph.FunctionNode) for n in node.out_nodes[0].out_nodes):
                self.remove_and_reconnect_neighbors(node.out_nodes[0])
        pass


def _visit(node: FuncGraph.Node, visited: set[FuncGraph.Node]):
    visited.add(node)
    for next_node in node.in_nodes:
        if next_node not in visited:
            _visit(next_node, visited)

    for next_node in node.out_nodes:
        if next_node not in visited:
            _visit(next_node, visited)


def _remove_subgraphs_not_connected_to_store(graph: FuncGraph, ce: counterexample.CE):
    visited = set()
    variable_names = list(map(lambda v: v.name, ce.store))
    var_nodes: List[FuncGraph.VariableNode] = list(filter(lambda n: isinstance(n, FuncGraph.VariableNode), graph.nodes))
    for node in var_nodes:
        if node.variable not in variable_names:
            continue
        if node in visited:
            continue

        _visit(node, visited)

    to_remove = []
    for node in graph.nodes:
        if node not in visited:
            to_remove.append(node)

    for node in to_remove:
        graph.remove_node(node)


def _remove_specific_function_calls(graph: FuncGraph, ce: counterexample.CE):
    to_remove = []
    for node in graph.nodes:
        if isinstance(node, FuncGraph.FunctionNode) and \
                (node.function.startswith("$FVF.loc_")
                 or node.function.startswith("$FVF.domain_")
                 or node.function.startswith("inv@")):
            to_remove.append(node)
            for out_node in node.out_nodes:
                to_remove.append(out_node)

    for node in to_remove:
        graph.remove_node(node)


def _main_functions_calls(statements: Iterable[smt_parser.Exp], ce: counterexample.CE) -> \
        ordered_set[smt_parser.FunctionApplication]:
    """
    extracts the top level user-defined function calls of the smt file
    top level = a user-defined function is a top level one, if it is the first one we encounter in a statement.

    example: two user-defined functions a and b.
    (assert (x (y (a 2) (b 4)))), here both a and b would be top level
    (assert (x (a (b 4)))), here only a is a top level function
    :param statements:
    """
    result: ordered_set[smt_parser.FunctionApplication] = ordered_set.OrderedSet()
    for statement in statements:
        if isinstance(statement, smt_parser.ForAllExpression) or isinstance(statement, smt_parser.Constant) \
                or isinstance(statement, smt_parser.Variable):
            continue
        elif isinstance(statement, smt_parser.BracketExpression) and len(statement.elements) > 0 \
                and statement.elements[0].smt_string in ["define-fun", "exists", "define-sort", "define-sort",
                                                         "declare-sort", "declare-const", "define-const",
                                                         "declare-datatypes", "set-option", "get-info"]:
            continue

        elif isinstance(statement, smt_parser.FunctionApplication) \
                and statement.name in ce.sources.smt_file.function_declarations:
            result.add(statement)

        elif isinstance(statement, smt_parser.BracketExpression):
            result.union(_main_functions_calls(statement.elements, ce))
        else:
            logging.warning("main_function_calls: unexpected statement" + str(statement))

    return result


def build_function_application_graph(ce: counterexample.CE, solver: Z3Solver, simplified: bool = False):
    g: FuncGraph = FuncGraph(ce)

    for func_application in _main_functions_calls(ce.sources.smt_file.statements, ce):
        g.build_subgraph_from_function_call(func_application, ce.sources.z3_model, solver)

    foralls = search_all_forall_expressions(ce.sources.smt_file.statements, solver)
    func_calls = ce.sources.smt_file.call_dict

    # for i in range(1, 3):
    new_statements: Set[smt_parser.FunctionApplication] = instantiate_forall_expressions(func_calls, foralls,
                                                                                         solver)
    for func_application in _main_functions_calls(new_statements, ce):
        g.build_subgraph_from_function_call(func_application, ce.sources.z3_model, solver)

        # if func_application.name in func_calls:
        #    func_calls[func_application.name].add(func_application)
        # else:
        #    func_calls[func_application.name] = set([func_application])
    #    logging.debug("iteration " + str(i) + " finished")

    _remove_specific_function_calls(g, ce)

    if simplified:
        g.simplify_graph()

    _remove_subgraphs_not_connected_to_store(g, ce)

    ce.function_graph = g
