import logging
import time
from typing import Set

import function_application_graph
from counterexample import CE
from inputparsing.z3_model_parser import *


def get_user_defined_name(internal_name: str):
    if "@" in internal_name:
        return internal_name.split("@")[0]
    return internal_name


def is_reference(var_type: str) -> bool:
    return var_type == "$Ref"


def _sanitize(s: str) -> str:
    return s.replace("<", "&lt;").replace(">", "&gt;")


class Visualisation:

    def __init__(self):
        self.cluster_id = 0
        self.dot_img = ""
        self.heap_clusters: Dict[str, str] = {}
        self.first_node_of_cluster: Dict[str, str] = {}
        self.variable_nodes: Set[str] = set()

    def _cluster_exists(self, name) -> bool:
        return name in self.heap_clusters

    def _get_cluster_id(self, name: str) -> str:
        if name not in self.heap_clusters:
            self.heap_clusters[name] = "cluster_" + str(self.cluster_id)
            self.cluster_id += 1
        return self.heap_clusters[name]

    def _add_node_to_subgraph(self, cluster_id, node_id, node_label) -> str:
        if cluster_id not in self.first_node_of_cluster:
            self.first_node_of_cluster[cluster_id] = node_id
        return "\"{node_id}\" [label=\"{label}\"];\n".format(
            node_id=node_id,
            label=node_label
        )

    def _add_html_node_to_subgraph(self, cluster_id, node_id, html_label) -> str:
        if cluster_id not in self.first_node_of_cluster:
            self.first_node_of_cluster[cluster_id] = node_id
        return "\"{node_id}\" [label={html}];\n".format(
            node_id=node_id,
            html=html_label
        )

    def _add_reference_fields(self, heap: CE.Heap) -> str:
        dot_img = ""

        heap_variables = {(item.heap_chunk, item.heap_chunk_model) for item in heap.permissions}

        # build subgraphs
        for t in heap_variables:
            var = t[0]
            smt_variable = t[1]
            cluster_id = self._get_cluster_id(smt_variable)

            dot_img += "subgraph \"{cluster_id}\" {{\n".format(cluster_id=cluster_id)
            if not var.startswith("$"):
                display = get_user_defined_name(var)
            else:
                display = smt_variable
            dot_img += "label = \"{var}\";\n".format(var=display)

            items: List[CE.Permission] = [item for item in heap.permissions if item.heap_chunk == var]
            for perm in items:
                node_id = "{cluster_id}_{field}".format(cluster_id=cluster_id, field=perm.field)

                # TODO currently the perm.value_type for permission fields will always be "None".
                #  Parse the viper file to extract the field type
                #  Quick fix with the model_value.startswith("$Ref")

                if is_reference(perm.value_type) or (
                        isinstance(perm.model_value, str) and perm.model_value.startswith("$Ref!")):
                    label = "{field}".format(field=perm.field)
                else:
                    label = "{field} = {smt_value}".format(field=perm.field, smt_value=(
                        perm.symbolic_value if perm.model_value is None else perm.model_value))
                dot_img += self._add_node_to_subgraph(self._get_cluster_id(smt_variable), node_id, label)

            dot_img += "}\n"
        return dot_img

    def _add_quantified_permission_fields(self, heap: CE.Heap):
        dot_img = ""
        for qp in heap.quantified_permissions:
            dot_img += self._add_function_subgraph(qp.func)

        return dot_img

    def _add_variables(self, variables: List[CE.Variable]):
        dot_img = ""
        # add nodes
        for var in variables:

            display = get_user_defined_name(var.name)
            if not is_reference(var.type):
                dot_img += "\"{node}\" [label=\"{disp}: {type} = {value}\"]\n".format(node=var.name,
                                                                                      type=var.type,
                                                                                      disp=display,
                                                                                      value=var.model_value)

                self.variable_nodes.add(var.name)
            else:
                dot_img += "\"{node}\" [label=\"{disp}: {type}\"]\n".format(node=var.name, disp=display, type=var.type)
                dot_img += self._add_reference_edge(var.name, var.model_value, constraint=True)
        return dot_img

    def _add_reference_edge(self, source, reference, constraint) -> str:
        target_cluster = self._get_cluster_id(reference)
        if target_cluster not in self.first_node_of_cluster:
            logging.warning("expected {tar}, but did not found it".format(tar=target_cluster))
            return ""

        target_node = self.first_node_of_cluster[target_cluster]

        return self._add_edge_to_subgraph(source, target_node, target_cluster, constraint)

    def _add_edge(self, source: str, target: str, constraint: bool = True, style: str = "") -> str:
        return "\"{src}\" -> \"{tar}\" [{opt} {style}]\n" \
            .format(src=source, tar=target, opt="" if constraint else "constraint=false", style=style)

    def _add_edge_to_subgraph(self, source, target_node, target_cluster, constraint=True) -> str:
        return "\"{src}\" -> \"{tar}\" [lhead=\"{cluster}\" {opt}]\n" \
            .format(src=source, tar=target_node, cluster=target_cluster,
                    opt="constraint=true" if constraint else "constraint=false")

    def _add_function_subgraph(self, func: CE.Function) -> str:
        # name: str, func: Function, function_declaration: Tuple[List[str], str]
        dot_img = "subgraph \"cluster_{cluster_id}\" {{\n".format(cluster_id=func.name)
        dot_img += "label = \"{var}\";\n".format(var=func.name)
        cases_list = []

        if len(func.cases) > 1:
            for case in func.cases:
                node_id = "{cluster_id}_{field_nr}".format(cluster_id=func.name,
                                                           field_nr=len(cases_list))
                # TODO fix this repr requirement
                entries = "<TD>" + ", </TD><TD>".join(map(lambda x: _sanitize(repr(x)), case[0])) + "</TD>"
                # TODO only show result if it is not a reference
                label = "<<TABLE BORDER=\"0\"><TR>{args}<TD>-&gt;</TD><TD>{result}</TD></TR></TABLE>>".format(
                    args=entries,
                    result=_sanitize(repr(case[1])))
                dot_img += self._add_html_node_to_subgraph(func.name, node_id, label)

                # if function_declaration[1] == "$Ref":
                # result = repr(case[1])
                # dot_img += self.add_reference_edge(node_id, result, constraint=True)

                cases_list.append(node_id)
        elif len(func.cases) == 1:
            node_id = "{cluster_id}_0".format(cluster_id=func.name)
            label = repr(func.cases[0][1])
            dot_img += self._add_node_to_subgraph(func.name, node_id, label)
        else:
            # raise ValueError("tried to visualise a function with 0 cases")
            node_id = "{cluster_id}_0".format(cluster_id=func.name)
            label = "empty function"
            dot_img += self._add_node_to_subgraph(func.name, node_id, label)

        dot_img += "}\n"
        return dot_img

    def _add_functions(self, ce: CE) -> str:
        dot_img = "subgraph cluster_functions {\n"
        dot_img += "label = \"Functions\"\n"
        dot_img += "rank=\"sink\"\n"

        for func in ce.functions:
            dot_img += self._add_function_subgraph(func)

        dot_img += "}\n"
        return dot_img

    def _add_heap(self, ce: CE) -> str:
        dot_img = "subgraph cluster_heap {\n"
        dot_img += "label = \"Heap\"\n"
        dot_img += "rank=\"same\"\n"

        dot_img += self._add_reference_fields(ce.heap)
        dot_img += self._add_quantified_permission_fields(ce.heap)

        for pred in ce.heap.predicate_instances:
            dot_img += "\"{entry}\" [label=\"{entry}({snap}; {args})\"]\n".format(entry=pred.predicate,
                                                                                  args=", ".join(pred.arguments),
                                                                                  snap=pred.snapshot)

        # add nodes for other entries
        for other in ce.heap.others:
            dot_img += "\"{entry}\" [label=\"{entry}\"]\n".format(entry=other.entry)
        dot_img += "}\n"
        return dot_img

    def _add_store(self, ce: CE) -> str:
        dot_img = "subgraph cluster_store {\n"
        dot_img += "label = \"Store\"\n"
        dot_img += "rank=\"source\"\n"

        dot_img += self._add_variables(ce.store)
        dot_img += "}\n"
        return dot_img

    def _add_edges(self, ce: CE) -> str:
        dot_img = ""

        # add edge from node inside subgraph to a subgraph
        for perm in ce.heap.permissions:
            # if this heap value points to a reference, add the edge
            if self._cluster_exists(perm.model_value):
                source = "{cluster_id}_{field}".format(
                    cluster_id=self._get_cluster_id(perm.heap_chunk_model),
                    field=perm.field)
                dot_img += self._add_reference_edge(source, perm.model_value, constraint=False)

        return dot_img

    def _add_graph_node(self, ce: CE, node: function_application_graph.FuncGraph.Node,
                        visited: Set[function_application_graph.FuncGraph.Node] = set()) -> \
            Tuple[str, str]:
        dot_img = ""
        edges = ""

        visited.add(node)

        node_id: str = ""
        if isinstance(node, function_application_graph.FuncGraph.VariableNode) \
                and node.variable in self.variable_nodes:
            # use the already defined node
            node_id = node.variable
        else:
            if isinstance(node, function_application_graph.FuncGraph.FunctionNode):
                # TODO special visualization for functions
                dot_img += "\"{node}\" [label=\"{disp}\" style=filled fillcolor=green]\n" \
                    .format(node=node.id,
                            disp=node.display_value())
            elif isinstance(node, function_application_graph.FuncGraph.ResultNode):
                # TODO special visualization for results
                dot_img += "\"{node}\" [label=\"{disp}\"]\n".format(node=node.id,
                                                                    disp=node.display_value())
            else:
                dot_img += "\"{node}\" [label=\"{disp}\"]\n".format(node=node.id,
                                                                    disp=node.display_value())
            node_id = node.id

        for out_node in node.out_nodes:
            # the source node adds the edge to the graph
            if isinstance(out_node, function_application_graph.FuncGraph.ResultNode):
                edges += self._add_edge(str(node_id), str(out_node.id),
                                        style="color=\"black:invis:black\" arrowhead=none")
            else:
                edges += self._add_edge(str(node_id), str(out_node.id))

            if out_node not in visited:
                d, e = self._add_graph_node(ce, out_node, visited)
                dot_img += d
                edges += e

        for in_node in node.in_nodes:
            if in_node not in visited:
                d, e = self._add_graph_node(ce, in_node, visited)
                dot_img += d
                edges += e

        return dot_img, edges

    def _add_function_graph(self, ce: CE) -> str:
        g: function_application_graph.FuncGraph = ce.function_graph
        visited: Set[function_application_graph.FuncGraph.Node] = set()

        dot_img = ""
        edges = ""

        for node in g.nodes:
            if node in visited:
                continue

            d, e = self._add_graph_node(ce, node, visited)
            dot_img += d
            edges += e

        return dot_img + edges

    def create_dot_file(self, ce: CE, include_function_graph: bool = True) -> str:
        start_time = time.time()

        dot_img = "digraph {\nnode [shape=rectangle];\n"
        dot_img += "rankdir=LR\n"
        dot_img += "compound=true;\n"

        dot_img += self._add_heap(ce)

        dot_img += self._add_store(ce)

        dot_img += self._add_functions(ce)

        dot_img += self._add_edges(ce)

        if include_function_graph and ce.function_graph is not None:
            dot_img += self._add_function_graph(ce)

        dot_img += "}\n"

        end_time = time.time()
        logging.debug("created dot file in " + str(round(end_time - start_time, 5)) + "s.")
        return dot_img
