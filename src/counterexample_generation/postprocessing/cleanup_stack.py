import logging
import re
from typing import List

CHECK_SAT = "(check-sat)"
PUSH = "(push) ; 1"


def find_declarations(content: List[str], start: int, end: int) -> (List[str], List[str]):
    result_set: List[str] = []

    for i in range(start, end):
        match = re.search("\\(declare-const.*", content[i])
        if match is not None:
            result_set.append(content[i])
            content[i] = ""
        match = re.search("\\(declare-fun.*", content[i])
        if match is not None:
            result_set.append(content[i])
            content[i] = ""

    return result_set, content


def cleanup_stack(content: List[str], start: int, end: int) -> str:
    if start == end:
        return "\n".join(content[start:end])

    # search the last pop statement
    for i in range(end - 1, start - 1, -1):
        match = re.search("\\(pop\\) ; (?P<index>\\d)", content[i])
        if match is None:
            continue

        pop_index = i
        break

    if match is None:
        return "\n".join(content[start:end])
    else:
        pop_nr = int(match.group("index"))

        # search corresponding push statement
        push_index = -1
        for i in range(pop_index - 1, start - 1, -1):
            match = re.search("\\(push\\) ; (?P<index>\\d)", content[i])
            if match is None:
                continue

            if int(match.group("index")) != pop_nr:
                continue

            push_index = i
            break

        # no matching push found
        if push_index == -1:
            exit(-1)

        # recursive call on the remaining part
        comp = [cleanup_stack(content, start, push_index), "\n".join(content[pop_index + 1:end])]
        return "\n".join(comp)


def run_cleanup_stack(lines: List[str]) -> List[str]:
    if len(lines) == 0:
        logging.error("cleanup_stack.py: read lines where empty")
        exit(1)

    if CHECK_SAT not in lines:
        logging.error("No '" + CHECK_SAT + "' found.")
        exit(2)

    if PUSH not in lines:
        logging.error("No '" + PUSH + "' found.")
        exit(3)

    first_push = lines.index(PUSH)

    last_check_sat = len(lines) - lines[::-1].index(CHECK_SAT) - 1

    declarations, lines = find_declarations(lines, first_push, last_check_sat)

    c = ["\n".join(lines[0:first_push]),
         "\n".join(declarations),
         cleanup_stack(lines, first_push, last_check_sat),
         "\n".join(lines[last_check_sat:])
         ]
    return c
