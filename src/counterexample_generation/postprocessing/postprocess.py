from typing import List
from pathlib import Path
from postprocessing import cleanup_stack


def postprocess(smt_file: Path) -> str:
    lines = smt_file.read_text().split("\n")
    lines = insert_get_model_after_last_check_sat(lines)
    lines = cleanup_stack.run_cleanup_stack(lines)
    return "\n".join(lines)


def remove_all_get_model_commands(lines = List[str]) -> List[str]:
    for i in range(0, len(lines)):
        if "(get-model)" in lines[i]:
            lines[i] = ""
    return lines


def insert_get_model_after_last_check_sat(lines: List[str]) -> List[str]:
    lines = remove_all_get_model_commands(lines)
    for i in range(len(lines)-1, -1, -1):
        if "(check-sat)" in lines[i]:
            lines[i] += "\n(get-model)"
            break
    return lines
