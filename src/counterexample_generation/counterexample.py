from typing import List, Tuple, Dict

import function_application_graph
import inputparsing.smt_parser as smt_parser
import inputparsing.symbex_log_parser as symbex_log_parser
import inputparsing.z3_model_parser as z3_model_parser


class CE:
    class Sources:
        def __init__(self, smt_file: smt_parser.SMTFile,
                     z3_model: z3_model_parser.Model,
                     symbex_heap: symbex_log_parser.SymbexLogHeap,
                     symbex_store: Dict[str, str],
                     method_name: str):
            self.smt_file = smt_file
            self.z3_model = z3_model
            self.symbex_heap = symbex_heap
            self.symbex_store = symbex_store
            self.failing_method = method_name

    class Variable:
        def __init__(self, var_type, name, model_value):
            self.type = var_type
            self.name = name
            self.model_value = model_value

    class Permission:
        def __init__(self, heap_chunk: str, heap_chunk_model: str, field: str, symbolic_value: str, model_value: str,
                     value_type: str,
                     perm_amount: str):
            self.heap_chunk = heap_chunk
            self.heap_chunk_model = heap_chunk_model
            self.field = field
            self.symbolic_value = symbolic_value
            self.model_value = model_value
            self.value_type = value_type
            self.perm_amount = perm_amount

    class QuantifiedPermission:
        def __init__(self, field: str, value: str, model_value: str, value_type: str, func):
            self.field = field
            self.value = value
            self.model_value = model_value
            self.value_type = value_type
            self.func: CE.Function = func

    class PredicateInstance:
        def __init__(self, predicate: str, snapshot: str, arguments: List[str]):
            self.predicate = predicate
            self.snapshot = snapshot
            self.arguments = arguments

    class OtherHeapEntry:
        def __init__(self, entry: str):
            self.entry = entry

    FunctionCase = Tuple[List[str], str]

    class Function:
        def __init__(self, name: str, par_list: Tuple[str], return_type: str, cases: List[z3_model_parser.FunctionCase]):
            self.name = name
            self.parameter_list = par_list
            self.return_type = return_type
            self.cases = cases

    class FunctionCall:
        def __init__(self, name, args: List[Tuple[str, str]], result):
            self.name = name
            self.arguments = args
            self.result = result

    class Heap:
        def __init__(self, permissions, qps, predicates, others):
            self.permissions: List[CE.Permission] = permissions
            self.quantified_permissions: List[CE.QuantifiedPermission] = qps
            self.predicate_instances: List[CE.PredicateInstance] = predicates
            self.others: List[CE.OtherHeapEntry] = others

    def __init__(self, smt_file: smt_parser.SMTFile, z3_model: z3_model_parser.Model,
                 symbex_heap: symbex_log_parser.SymbexLogHeap,
                 symbex_store: Dict[str, str],
                 method_name: str,
                 inverse_dict: Dict[str, smt_parser.Exp]):
        self.store: List[CE.Variable] = None
        self.heap: CE.Heap = None
        self.functions: List[CE.Function] = None
        self.function_graph: function_application_graph.FuncGraph = None
        self.sources = self.Sources(smt_file, z3_model, symbex_heap, symbex_store, method_name)
        self.inverse_dict = inverse_dict

    def simplify(self):
        if self.function_graph is not None:
            self.function_graph.simplify_graph()


