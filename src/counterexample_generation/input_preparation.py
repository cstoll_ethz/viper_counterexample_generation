import logging
import subprocess
import time
from pathlib import Path
from typing import List, Tuple, Iterable
import re
import os

from inputparsing.viper_client_log_parser import get_failing_methods, get_summary, parse
from postprocessing.postprocess import postprocess


def create_method_specific_smt_content(smt_file: Path, method_name: str) -> str:
    # the method sections in the smt file are separated by "; --------- method_name ----------"
    # split the file into sections (preamble, method1, method2, ..., methodN) and then concat the correct parts
    smt_lines = smt_file.read_text().split("\n")

    result = []
    region_start = 0
    found = False
    for i in range(1, len(smt_lines)):
        line = smt_lines[i]

        if line == "; End function- and predicate-related preamble":
            region_start = i + 1
            result += smt_lines[0:region_start]
            continue

        match = re.match(r"^; --+ (?P<method>[^\s]+) --+$", line)
        if match:
            if match.group("method") == method_name:
                found = True
            elif found:
                result += smt_lines[region_start:i]
                found = False
                break
            region_start = i

    # test if last region was the requested one
    if found:
        result += smt_lines[region_start:]

    return "\n".join(result)


def run_z3(smt_file: Path) -> str:
    try:
        return subprocess.check_output(
            ["z3", "model.completion=true", "model.partial=true", "pp.min_alias_size=4294967295", "pp.single_line=true",
             "pp.max-depth=4294967295", str(smt_file)]) \
            .decode("utf-8")
    except subprocess.CalledProcessError as e:
        logging.error(e)
        exit(-1)


def run_postprocess(method_dir: Path) -> Path:
    if not method_dir.is_dir():
        raise ValueError("method_dir was not a directory")
    if not (method_dir / "method.raw.smt2").exists():
        raise ValueError(str(method_dir / "method.raw.smt2") + " not found")

    smt_file = method_dir / "method.raw.smt2"
    new_smt_file = method_dir / "method.smt2"

    result = postprocess(smt_file)
    new_smt_file.write_text(result)

    return new_smt_file


def prepare(test_case: Path) -> List[str]:
    prep_time_start = time.time()

    viper_client_log: Path = test_case / "viper_client.log"
    if not viper_client_log.exists():
        raise ValueError("viper_client.log not found (" + str(viper_client_log) + ")")

    # errors: List[Tuple[str, str]]
    start_time = time.time()
    errors, summary = parse(viper_client_log)
    if len(errors) == 0:
        return []
    end_time = time.time()
    logging.debug("parsing viper_client.log took " + str(round(end_time - start_time, 5)) + "s")

    summary_file = test_case / "error_summary.json"
    summary_file.write_text(summary)

    smt_file: Path = test_case / "raw.smt2"
    if not smt_file.exists():
        raise ValueError("raw.smt2 not found (" + str(smt_file) + ")")

    symb_ex_log: Path = test_case / "symbExLog.json"
    if not symb_ex_log.exists():
        raise ValueError("symbExLog.json not found (" + str(symb_ex_log) + ")")

    # create directories and method specific files
    for name, error in errors:
        logging.info("Found verification error in function/method '" + name + "'")
        method_dir = test_case.resolve() / name
        method_dir.mkdir(parents=True, exist_ok=True)

        error_json_file = method_dir / "error.json"
        error_json_file.write_text(error)

        content = create_method_specific_smt_content(smt_file, name)
        method_smt_file = test_case / name / "method.raw.smt2"
        method_smt_file.write_text(content)

        new_smt_file = run_postprocess(method_dir)
        if os.path.getsize(str(new_smt_file.absolute())) == 0:
            raise ValueError("new smt file is empty")

        z3_output_file = test_case / name / "z3.out"
        z3_output = run_z3(new_smt_file)

        # quick fix for a nicer latex listing
        z3_output = z3_output.replace("  ", " ")

        z3_output_file.write_text(z3_output)

    prep_time_end = time.time()
    logging.debug("Input preparation phase finished after " + str(round(prep_time_end - prep_time_start, 5)) + "s.")

    return [method for method, _ in errors]
