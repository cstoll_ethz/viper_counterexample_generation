import fileinput
import re
import sys

# small helper script for search/replace functionality in the shell scripts

print(re.sub(sys.argv[1],sys.argv[2], sys.stdin.read()))


