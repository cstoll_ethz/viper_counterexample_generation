import sys
import json
import os

# usage: mark_failure_in_viper_code.py "error_file.json" "code.vpr"

if len(sys.argv) < 3:
    print("missing arguments. Usage: mark_failure_in_viper_code.py 'error_file.json' 'code.vpr'")
    exit(-1)

error_file = sys.argv[1]
viper_file = sys.argv[2]

with open(error_file, "r") as f:
    error_str = f.read()

with open(viper_file, "r") as f:
    viper_code = f.read()

if error_str == "":
    errors = []
else:
    obj = json.loads(error_str)
    if "result" in obj and "errors" in obj["result"]:
        errors = obj["result"]["errors"]
    else:
        errors = ""

underline_str = r"[*"

lines = viper_code.split("\n")

for error in errors:
    start_line_nr = int(error["position"]["start"].split(":")[0]) - 1
    start_character = int(error["position"]["start"].split(":")[1]) - 1

    end_line_nr = int(error["position"]["end"].split(":")[0]) - 1
    end_character = int(error["position"]["end"].split(":")[1]) - 1

    startline = lines[start_line_nr]
    lines[start_line_nr] = startline[0:start_character] + underline_str + startline[start_character:]

    endline = lines[end_line_nr]
    end_character = end_character + len(underline_str)
    lines[end_line_nr] = endline[0:end_character] + "*]" + endline[end_character:]

    #escape { and } inside the marks

content = "\n".join(lines)

filename = os.path.splitext(viper_file)[0]
with open(filename+".tex", "w") as f:
    f.write(content)
