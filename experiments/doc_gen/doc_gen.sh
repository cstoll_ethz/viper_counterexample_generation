#!/bin/bash

if [ -z "$1" ]; then
		echo "usage: doc_gen [--smt] 'example_folder'"
		echo "--smt : removes the smt from the output"
		exit -1;
fi

RED='\033[0;31m'
NC='\033[0m'
GREEN='\033[0;32m'

#default settings
print_smt=true

while :; do
    case $1 in
        -smt) print_smt="false"
        ;;
        +smt) print_smt="true"
        ;;
        *)  if [[ ! -d "$1" ]]; then
				echo -e "${RED}expected a path, but received '$1'${NC}"
				exit -1
			fi
			test_suite="$(realpath "$1")/"
			break
    esac
    shift
done

suite_name="${test_suite%*/}"
suite_name="${suite_name##*/}"

#switch working directory
cd "$(dirname "$0")"

tmp_dir="tmp/"
if [ ! -d "${tmp_dir}" ]; then
		mkdir "${tmp_dir}"
else
	rm ${tmp_dir}*
fi

if [ ! -d "${test_suite}doc" ]; then
	mkdir "${test_suite}doc"
fi

#replace_with_file REPLACE_FILE REPLACE_STRING CONTENT_FILE
# REPLACE_FILE is the file in which the replace will take place
# REPLACE_STRING the string that will get replaced
# CONTENT_FILE the content of this file will be inserted
function replace_with_file()
{
	if [ ! -f "$1" ]; then
		echo -e "${RED}replace_with_file: Did not find the REPLACE_FILE '$1'${NC}"
		exit -1
	fi
	if [ ! -f "$3" ]; then
		echo -e "${RED}replace_with_file: Did not find the CONTENT_FILE '$3'${NC}"
		exit -1
	fi
	awk -i inplace -v old="$2" -v new="$(cat "$3" | sed 's/\\/\\\\/g')" 's=index($0,old){$0=substr($0,1,s-1) new substr($0,s+length(old))} 1' "$1"
}

# testcase NAME
# NAME must be a testcase under TEST_SUITE/results/NAME
function testcase()
{
	name="$1"
	test_dir="${test_suite}results/${name}/"

	if [ ! -d "${test_dir}" ]; then
		echo -e "${RED}Invalid testcase: '${test_dir}'"
		exit -1
	fi

	case_tex="${tmp_dir}${name}.tex"

	cp "template/case.tex" "${case_tex}"

	#Name / Title
	sed -i "s/TESTCASE/${name}/" ${case_tex}

	case_desc_file="${test_suite}doc/${name}.tex"
	if [ ! -f "${case_desc_file}" ]; then
		echo "" > "${case_desc_file}"
	fi
	replace_with_file "${case_tex}" "DESCRIPTION" "${case_desc_file}"
	
	#fix _ in latex file (only in the text parts)
	sed -i 's/_/\\_/g' ${case_tex}

	#include viper error messages
	error_file="${test_dir}error_summary.json"
	python3 "get_errors.py" "${error_file}" > "${tmp_dir}error"
	replace_with_file "${case_tex}" "ERROR" "${tmp_dir}error"
	
	#Viper Code
	#----------
	viper_file="${test_dir}code.vpr"
	viper_tex="${test_dir}code.tex"
	#mark the error positiom
	python3 "mark_failure_in_viper_code.py" "${error_file}" "${viper_file}"
	#copy the file
	replace_with_file "${case_tex}" "CODE.VPR" "${viper_tex}"

	for m in $(find "${test_dir}" -maxdepth 1 -mindepth 1 -type d -printf '%f\n' | sort)
	do
		method_tex="${tmp_dir}${name}_${m}.tex"
		cp "template/method.tex" "${method_tex}"

		sed -i "s/METHODNAME/${m}/" "${method_tex}"
		method_dir="${test_dir}${m}/"
		sed -i 's/_/\\_/g' ${method_tex}
		
		#Processed SMT2 Output
		if [ "${print_smt}" = true ]; then	
			processed_smt2="${method_dir}method.smt2"
			replace_with_file "${method_tex}" "PROCESSED.SMT2" "${processed_smt2}"
		fi

		#Z3 Output
		z3_output="${method_dir}z3.out"
		replace_with_file "${method_tex}" "OUTPUT.Z3" "${z3_output}"
	
		#Counterexample
		counterexample="${method_dir}/counterexample.txt"
		# replace_with_file "${method_tex}" "CETEXT" "${counterexample}"
		
		counterexample_img="\{${method_dir}counterexample.dot\}.png"
		sed -i "s;CEIMG;${counterexample_img};" "${method_tex}"

		#Add include to case.tex
		content="\\\\input{$(basename "${method_tex}")}"
		awk -i inplace "FNR==NR{ if (/% METHODCASE/) p=NR; next} FNR==p{ print \"${content}\" }; {print}" "${case_tex}" "${case_tex}"
	done

		
	#LABEL
	sed -i "s/LABEL/${name}/" ${case_tex}

	#Add include to main.tex
	content="\\\\input{$(basename "${case_tex}")}"
	awk -i inplace "FNR==NR{ if (/% CASE/) p=NR; next} FNR==p{ print \"${content}\" }; {print}" ${tmp_dir}main.tex ${tmp_dir}main.tex

}

main=${tmp_dir}main.tex
cp "template/main.tex" "${main}"
cp "template/basics.sty" "${tmp_dir}"
sed -i "s/TEST/${suite_name}/" "${main}"

#DESCRIPTION
desc_file="${test_suite}doc/desc.tex"
if [ ! -f "${desc_file}" ]; then
	touch "${desc_file}"
fi
replace_with_file "${main}" "DESCRIPTION" "${desc_file}"

#fix _ in main latex file
sed -i 's/_/\\_/g' ${main}

# include postprocess.sh
# postprocess_file="${test_suite}postprocess.sh"
# replace_with_file "${main}" "POSTPROCESS" "${postprocess_file}"

for d in $(find ${test_suite}results/ -maxdepth 1 -mindepth 1 -type d -printf '%f\n' | sort)
do
	testcase "$d"
done

pdflatex_args="-interaction=batchmode -halt-on-error"
(cd "${tmp_dir}"
	#generate pdf
	pdflatex ${pdflatex_args} "main.tex"
	if [ $? -ne 0 ]; then
		exit -1
	fi
	pdflatex ${pdflatex_args} "main.tex"
	if [ $? -ne 0 ]; then
		exit -1
	fi
)

if [ $? -ne 0 ]; then
	echo -e "${RED}Error during pdflatex, look at the log file under doc_gen/tmp/main.log.${NC}"
	exit -1
fi

cp ${tmp_dir}main.pdf ${test_suite}${suite_name}.pdf
