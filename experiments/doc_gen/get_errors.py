import sys
import json
import os

if len(sys.argv) < 2:
    print("missing arguments. Usage: get_errors.py 'error_file.json'")
    exit(-1)

error_file = sys.argv[1]

with open(error_file, "r") as f:
    error_str = f.read()

if error_str == "":
    errors = []
else:
    obj = json.loads(error_str)
    if "result" in obj and "errors" in obj["result"]:
        errors = obj["result"]["errors"]
    else:
        errors = ""

lines = []


for error in errors:
    lines.append("Error [Line " + error["position"]["start"].split(":")[0] + "]: "+ error["text"])

if errors == []:
    lines.append("No errors detected.")

result = "\\\\".join(lines)
result = result.translate(str.maketrans({"_":  r"\_",
                                          "{":  r"\{",
                                          "}":  r"\}",
                                          "^":  r"\^",
                                          "$":  r"\$",
                                          "&":  r"\&",
                                          }))

print(result)
