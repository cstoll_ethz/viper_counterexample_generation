field val : Int // integer arrays

define access(a) forall j: Int :: 0 <= j && j < len(a) ==> acc(loc(a, j).val)
define untouched(a) forall j: Int :: 0 <= j && j < len(a) ==> loc(a, j).val == old(loc(a, j).val)

domain Array {
    function loc(a: Array, i: Int): Ref
    function len(a: Array) : Int
    function first(r: Ref) : Array
    function second(r: Ref) : Int

    axiom injectivity {
        forall a: Array, i: Int :: {loc(a, i)} first(loc(a, i)) == a && second(loc(a, i)) == i
    } 

    axiom length_nonneg {
        forall a: Array :: len(a) >= 0
    }
}

method find_max(a:Array) returns (max:Int)
    requires access(a)
    requires forall i:Int :: 0 <= i && i < len(a) ==> loc(a, i).val >= 0
    requires len(a) == 2

    ensures access(a) && untouched(a)
    ensures len(a) == 0 ==> max == -1
    ensures forall i:Int :: 0 <= i && i < len(a) ==> loc(a, i).val <= max
{
    if(len(a) == 0) 
    {
        max := -1
    }
    else
    {
        max := 0
        var i:Int := 0
        var last: Int := 0
        while(i < len(a))
            invariant i >= 0 && i <= len(a)
            invariant access(a) && untouched(a)
            invariant forall j:Int :: { loc(a, j) } j >= 0 && j < i ==> loc(a, j).val <= max
            invariant i > 0 ==> last == loc(a, i-1).val
            invariant i == 0 ==> last == 0 && max == 0
        {
            if(loc(a, i).val > last)
            {
                max := loc(a, i).val
            }
            last := loc(a, i).val
            i := i + 1
        }
    }
}
