field a : Bool
field b : Bool

function inc(config:Ref, x:Int): Int
    requires acc(config.a, 1/2) && acc(config.b, 1/2)
    ensures result == (config.a || !config.b ? x + 1 : x)
{
    config.a || !config.b ? x + 1 : x
}

method access(r:Ref, r2:Ref)
    requires acc(r.a) && acc(r.b)
    requires acc(r2.a) && acc(r2.b)
    requires r.a == true && r.b == true
{
    assert inc(r, 5) == 6
    assert inc(r, 7) == 8
    r.a := false
    assert inc(r, 5) == 5
    assert inc(r, 7) == 7

    assert inc(r2, 7) == 8
}