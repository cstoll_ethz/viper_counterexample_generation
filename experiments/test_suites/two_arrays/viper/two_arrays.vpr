field val : Int // integer arrays

define access(a) forall j: Int :: { loc(a, j) } 0 <= j && j < len(a) ==> acc(loc(a, j).val)
define untouched(a) forall j: Int :: { loc(a, j) } 0 <= j && j < len(a) ==> loc(a, j).val == old(loc(a, j).val)

domain Array {
    function loc(a: Array, i: Int): Ref
    function len(a: Array) : Int
    function first(r: Ref) : Array
    function second(r: Ref) : Int

    axiom injectivity {
        forall a: Array, i: Int :: {loc(a, i)} first(loc(a, i)) == a && second(loc(a, i)) == i
    } 

    axiom length_nonneg {
        forall a: Array :: len(a) >= 0
    }
}

method equal(a:Array, a2: Array) returns (eq: Bool)
    requires access(a) && access(a2)

    ensures access(a) && access(a2)
    ensures untouched(a) && untouched(a2)
    ensures (len(a) != len(a2)) ==> !eq
    ensures (len(a) == len(a2) && (forall j:Int :: {loc(a, j)} j >= 0 && j < len(a) ==> loc(a, j).val == loc(a2, j).val)) == eq
{
    
    eq := true
    if(len(a) != len(a2))
    {
        eq := false
    }
    else
    {
        var i:Int := 0
        while(i < len(a))
            invariant i >= 0 && i <= len(a)
            invariant access(a) && untouched(a)
            invariant access(a2) && untouched(a2)
            invariant len(a) == len(a2)
            invariant (forall j:Int :: {loc(a, j)} j >= 0 && j < i ==> loc(a, j).val == loc(a2, j).val) == eq
        {
            if(loc(a, i).val != loc(a2, i).val)
            {
                eq := false
            }
            i := i + 1
        }
    }
}

method test_incorrect(a:Array, a2: Array)
    requires access(a) && access(a2)
    requires forall i:Int :: {loc(a, i)} 0 <= i && i < len(a) ==> loc(a, i).val >= 0
    requires forall i:Int :: {loc(a, i)} 0 <= i && i < len(a2) ==> loc(a2, i).val >= 0

{
    assume len(a) == 2 && loc(a, 0).val == 1 && loc(a, 1).val == 2
    assume len(a2) == len(a)

    var e: Bool
    e := equal(a, a2)
    assert e
}


method test_correct(a:Array, a2: Array)
    requires access(a) && access(a2)
{
    assume len(a) == 2 && loc(a, 0).val == 1 && loc(a, 1).val == 2
    assume len(a2) == 2 && loc(a2, 0).val == 1 && loc(a2, 1).val == 2

    var e: Bool
    e := equal(a, a2)
    assert len(a) == 2 && loc(a, 0).val == 1 && loc(a, 1).val == 2
    assert len(a2) == 2 && loc(a2, 0).val == 1 && loc(a2, 1).val == 2
    assert e
}