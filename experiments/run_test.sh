#!/bin/bash
cd "$(dirname "$0")"
usage="usage: run_test.sh TEST_DIR"

RED='\033[0;31m'
NC='\033[0m'
GREEN='\033[0;32m'

search_replace="old_postprocess/search_replace.py"

#default settings
add_smt_to_pdf=true
print_output=true

while :; do
    case $1 in
		-output) print_output=false
		;;
		+output) print_output=true
		;;
        -smt) add_smt_to_pdf="false"
        ;;
        +smt) add_smt_to_pdf="true"
        ;;
        *)  if [[ ! -d "$1" ]]; then
				echo -e "${RED}expected a path, but received '$1'${NC}"
				exit -1
			fi
			test_dir="$(realpath "$1")/"
			break
    esac
    shift
done

if [ -z "$1" ]; then
	echo "${usage}"
	exit -1
fi

if [ ! -d "$1" ]; then
	echo -e "${RED}Directory not found '$1'${NC}"
	exit -1
fi

examples_dir="./"

results_dir="${test_dir}results/"
if [ -d "${results_dir}" ]; then
		rm -r "${results_dir}"
fi
mkdir "${results_dir}"

test_suite_name=$(basename "${test_dir}")

while IFS= read -r -d '' file; do
	[ -e "$file" ] || exit -1

	name=$(basename "$file" ".vpr")
	echo -e "${GREEN}Execute testcase ${test_suite_name}:${name}${NC}"

	test_case_dir="${results_dir}${name}/"
	mkdir "${test_case_dir}"

	viper_file=$(realpath "${file}")

	start_time=$(date +%s.%N)
	output=$(./../src/generate_counterexample.sh "${viper_file}" "${test_case_dir}")
	failed=$?

	if [ "${print_output}" = true ]; then
		echo "${output}"
	fi
	if (($failed)); then
		echo -e "${RED}Error while generate_counterexample.sh.${NC}"
		exit -1
	fi
	end_time=$(date +%s.%N)
	execution_time=$(echo "scale=4; (${end_time} - ${start_time})*10000/10000" | bc)
	echo -e "${GREEN}Counterexample generated in ${execution_time}s.${NC}"
	
	
done < <(find "${test_dir}viper/" -type f -name '*.vpr' -print0)

# latex report currently disabled (start manually if interested)
# echo -e "${GREEN}Start documentation generation process${NC}"
# if [ "$add_smt_to_pdf" = true ]; then
#   args=""
# else
#   args="-smt"
# fi
# ${examples_dir}doc_gen/doc_gen.sh $args "${test_dir}"

