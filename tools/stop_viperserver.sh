#!/bin/bash

RED='\033[0;31m'
NC='\033[0m'
GREEN='\033[0;32m'

# Usage
# stop_viperserver.sh [PORTNUMBER]

port=7770
#check for optional port number
if [ -n "$1" ]; then
  port=$1
fi

netstat_line=$(netstat -tulpn 2> /dev/null | grep "127.0.0.1:$port")

if [ -n "$netstat_line" ]; then
  PID=$(echo $netstat_line | awk '{print($7)}' | cut -d'/' -f1)

  kill $PID
  echo -e "${GREEN}Server stopped${NC}"
else
  echo -e "${RED}No server to stop (behind port $port).${NC}"
fi
