#!/bin/bash
cd "$(dirname "$0")"
RED='\033[0;31m'
NC='\033[0m'
GREEN='\033[0;32m'

tmp_dir="/tmp/viper"
z3_log="output.log"
server_log="server_startup.log"

if [ ! -d "${tmp_dir}" ]; then
	mkdir "${tmp_dir}"
fi

port=7770

if [[ -f "viper.jar" ]]; then
	classpath=viper.jar
else
	classpath=/usr/local/Viper/backends/*
fi
echo $classpath
echo -e "${GREEN}Start viperserver on port $port${NC}"

java -Xmx2048m -Xss16m -cp "${classpath}" -server viper.server.ViperServerRunner --logLevel ALL --port $port --logFile="${tmp_dir}" > ${tmp_dir}/${server_log} &


