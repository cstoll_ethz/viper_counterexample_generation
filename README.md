Master Thesis Codebase "SMT Models for Verification Debugging"
==============================================================

## Requirements
 * Python Version 3.7 (as python3.7) [+ Python3 for viper_client]
 * install Requests for Python3 (viper_client dependency, "pip3 install requests")
 * Z3 (used version 4.8.5, 64 bit)
 * Java (used version java 11.0.1)
 * Scala
 * VS Code -> Install Viper
 * graphviz

## Generate a Counterexample:
 * Start the viper server "tools/start_viperserver.sh"
 * In the "src" directory execute "generate_counterexample.sh"
    ./generate_counterexample.sh VIPERFILE [OUTPUT_DIRECTORY]

If you don't specify a output directory "out" will be used. This directory will then contain the main generated input files (SMT, SymbExLog, ViperClient output) and a directory for each failing method/function. In each directory will be the method specific SMT file, SymbExLog and Z3 outputs. Also you will find the generated counterexample in this directory (both the json and the visualization).



